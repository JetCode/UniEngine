package ir.markazandroid.UniEngine.conf.sql;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ali on 5/17/2018.
 */
@Configuration
public class SQLiteConfiguration {

    @Bean("sqliteDataSource")
    @ConfigurationProperties("sqlite.datasource")
    public DataSource dataSource(Environment env) throws SQLException {
        DataSource dataSource = DataSourceBuilder
                .create()
                .url(env.getProperty("sqlite.datasource.url"))
                .driverClassName("org.sqlite.JDBC")
                .build();

        try (Connection connection = dataSource.getConnection()) {
            connection.createStatement().execute(
                    "create table if not exists files(" +
                    "file_id text," +
                    "reference text," +
                    "primary key (file_id,reference)" +
                    ") ");
        }
        return dataSource;
    }

    @Bean("sqliteEntityManager")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                       @Qualifier("sqliteDataSource") DataSource dataSource) {

        Map<String, String> props = new HashMap<>();
        props.put("hibernate.dialect", "org.hibernate.dialect.SQLiteDialect");
        return builder
                .dataSource(dataSource)
                .properties(props)
                .packages("ir.markazandroid.UniEngine.persistance.file.entity")
                .persistenceUnit("fileMetaData")
                .build();
    }


    @Bean("sqlite")
    public PlatformTransactionManager transactionManager(@Qualifier("sqliteEntityManager") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
