package ir.markazandroid.UniEngine.conf.sql;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Created by Ali on 5/17/2018.
 */
@Configuration
public class MainDBConfiguration {

    @ConfigurationProperties("main.datasource")
    @Bean("mainDataSource")
    @Primary
    public DataSource dataSource(Environment env) {
        return DataSourceBuilder
                .create()
                .url(env.getProperty("main.datasource.url"))
                .driverClassName(env.getProperty("main.datasource.driver-class-name"))
                .username(env.getProperty("main.datasource.username"))
                .password(env.getProperty("main.datasource.password"))
                .build();
        //dataSource.setDataSourceJNDI(env.getProperty("main.datasource.jndi-name"));
        //DriverManagerDataSource dataSource = new DriverManagerDataSource();
    }

    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder, DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("ir.markazandroid.UniEngine.persistance.entity")
                .persistenceUnit("UniEngineMain")
                .build();
    }


    @Primary
    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

}
