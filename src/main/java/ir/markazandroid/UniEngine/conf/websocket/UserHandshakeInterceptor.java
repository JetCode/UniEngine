package ir.markazandroid.UniEngine.conf.websocket;

import ir.markazandroid.UniEngine.conf.session.SessionManager;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

/**
 * Created by Ali on 2/4/2019.
 */
@Component
public class UserHandshakeInterceptor implements HandshakeInterceptor {

    private final SessionManager sessionManager;

    public UserHandshakeInterceptor(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }
    // private final PhoneService phoneService;

   /* @Autowired
    public UserHandshakeInterceptor(PhoneService phoneService) {
        this.phoneService = phoneService;
    }*/


    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        //System.out.println("req");
        if (request instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest servletRequest
                    = (ServletServerHttpRequest) request;
            Object principal = sessionManager.getPrincipal(servletRequest.getServletRequest().getSession());
            if (!(principal instanceof UserEntity))
                return false;

            attributes.put("userEntity", principal);
            return true;
        }
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

    }
}
