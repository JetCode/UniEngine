package ir.markazandroid.UniEngine.conf.websocket;

import ir.markazandroid.UniEngine.JSONParser.JsonMessageConverter;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.List;
import java.util.Map;

/**
 * Created by Ali on 1/29/2019.
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    private final UserHandshakeInterceptor userHandshakeInterceptor;
    private final DeviceHandshakeInterceptor deviceHandshakeInterceptor;
    private final JsonMessageConverter messageConverter;
    private final TaskScheduler poolScheduler;
    private final Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);

    @Autowired
    public WebSocketConfig(UserHandshakeInterceptor userHandshakeInterceptor, DeviceHandshakeInterceptor deviceHandshakeInterceptor, JsonMessageConverter messageConverter, TaskScheduler poolScheduler) {
        this.userHandshakeInterceptor = userHandshakeInterceptor;
        this.deviceHandshakeInterceptor = deviceHandshakeInterceptor;
        this.messageConverter = messageConverter;
        this.poolScheduler = poolScheduler;
    }

/*
    @Component
    public class SessionDisconnectEventListener implements ApplicationListener<SessionDisconnectEvent> {

        @Override
        public void onApplicationEvent(SessionDisconnectEvent event) {
            System.out.println(event.toString());
            //Logger.debug("Connect event [sessionId: " + sha.getSessionId() +"; company: "+ company + " ]");

        }
    }*/

    /*@Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registry) {
        registry.addDecoratorFactory(new WebSocketHandlerDecoratorFactory() {
            @Override
            public WebSocketHandler decorate(WebSocketHandler handler) {
               return new WebSocketHandlerDecorator(handler){
                   @Override
                   public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {

                   }
               };
            }
        });
    }*/

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/queue/user", "/queue/device")
                .setTaskScheduler(poolScheduler)
                .setHeartbeatValue(new long[]{5000, 5000});
        //config.setApplicationDestinationPrefixes();
        //config.setUserDestinationPrefix("/secured/user");
    }

    @Override
    public boolean configureMessageConverters(List<MessageConverter> converters) {
        converters.add(messageConverter);
        return false;
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {

        registry.addEndpoint("/socket/device").setHandshakeHandler(new DefaultHandshakeHandler() {
            @Override
            protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
                return () -> "device_" + ((DeviceEntity) attributes.get("deviceEntity")).getDeviceId();
            }
        }).addInterceptors(deviceHandshakeInterceptor);

        registry.addEndpoint("/socket/user/").setHandshakeHandler(new DefaultHandshakeHandler() {


            @Override
            protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
                //return super.determineUser(request, wsHandler, attributes);
                //System.out.println("user_"+((UserEntity)attributes.get("userEntity")).getUserId());
                return () -> "user_" + ((UserEntity) attributes.get("userEntity")).getUserId();
            }
        }).setAllowedOrigins("*")
                .addInterceptors(userHandshakeInterceptor).withSockJS().setHeartbeatTime(5000);

    }


  /*  @Override
    public void configureClientOutboundChannel(ChannelRegistration registration) {
        registration.interceptors(new ChannelInterceptor() {

            @Override
            public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
                logger.info("PostSend");
            }

            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                logger.info("PreSend");
                return message;
            }
        });
    }*/

}
