package ir.markazandroid.UniEngine.command;

import ir.markazandroid.UniEngine.JSONParser.Parser;
import ir.markazandroid.UniEngine.object.WebSocketMessage;
import ir.markazandroid.UniEngine.persistance.entity.CommandEntity;
import ir.markazandroid.UniEngine.service.interfaces.CommandService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Coded by Ali on 2/5/2019.
 */
@EnableScheduling
@Component
public class SocketMessageController {

    private ConcurrentHashMap<String, CommandEntity> commandQueue;
    private final CommandService commandService;
    private final Parser parser;
    private final SimpMessageSendingOperations messagingTemplate;


    @Autowired
    public SocketMessageController(CommandService commandService, Parser parser, SimpMessageSendingOperations messagingTemplate) {
        this.parser = parser;
        this.commandQueue = new ConcurrentHashMap<>();
        this.commandService = commandService;
        this.messagingTemplate = messagingTemplate;
    }

    @PostConstruct
    public void init() {
        List<CommandEntity> commandEntities = commandService.getUnReceivedCommands();
        commandEntities.parallelStream().forEach(commandEntity -> {
            commandEntity.setOwnMessage(parser.get(WebSocketMessage.class, new JSONObject(commandEntity.getOwnMessageJson())));
            commandQueue.put(commandEntity.getMessageId(), commandEntity);
        });
    }


    public boolean sendCommand(WebSocketMessage webSocketMessage) {
        AtomicBoolean replaced = new AtomicBoolean(false);
        commandQueue.forEachValue(1, commandEntity -> {
            if (commandEntity.getDeviceId() == webSocketMessage.getDeviceId() &&
                    commandEntity.getCmdCode() != null &&
                    commandEntity.getCmdCode().equals(webSocketMessage.getCmdCode())) {
                commandEntity.setStatus(CommandEntity.STATUS_CANCELED);
                commandService.updateCommand(commandEntity);
                commandQueue.remove(commandEntity.getMessageId());
                replaced.set(true);
            }
        });
        sendMessage(webSocketMessage);
        return replaced.get();
    }

    public void sendMessage(WebSocketMessage webSocketMessage) {
        CommandEntity commandEntity = new CommandEntity();
        commandEntity.setCommand(webSocketMessage.getMessage());
        commandEntity.setDeviceId(webSocketMessage.getDeviceId());
        commandEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
        commandEntity.setStatus(CommandEntity.STATUS_NOT_RECEIVED);
        commandEntity.setMessageId(webSocketMessage.getMessageId());
        commandEntity.setOwnMessage(webSocketMessage);
        commandEntity.setCmdCode(webSocketMessage.getCmdCode());
        commandEntity.setOwnMessageJson(parser.get(webSocketMessage).toString());

        commandService.saveCommand(commandEntity);

        sendM(commandEntity);
        commandQueue.put(webSocketMessage.getMessageId(), commandEntity);
    }

    public void messageReceived(WebSocketMessage webSocketMessage) {
        commandQueue.remove(webSocketMessage.getMessageId());
        CommandEntity commandEntity = commandService.getCommandByMessageId(webSocketMessage.getMessageId());
        if (commandEntity != null) {
            if (WebSocketMessage.ACK.equals(webSocketMessage.getType()) && commandEntity.getStatus() == CommandEntity.STATUS_NOT_RECEIVED) {
                commandEntity.setStatus(CommandEntity.STATUS_RECEIVED);
                commandEntity.setReceiveTime(new Timestamp(System.currentTimeMillis()));
                commandService.updateCommand(commandEntity);
            } else if (WebSocketMessage.RESPONSE.equals(webSocketMessage.getType())) {
                if (webSocketMessage.getSuccess())
                    commandEntity.setStatus(CommandEntity.STATUS_SUCCESS);
                else
                    commandEntity.setStatus(CommandEntity.STATUS_ERROR);

                commandEntity.setResult(webSocketMessage.getMessage());
                commandService.updateCommand(commandEntity);
            }
        }
    }

    public void sendWaitingMessagesToDevice(long deviceId) {
        commandQueue.forEachValue(1, commandEntity -> {
            if (commandEntity.getDeviceId() == deviceId)
                sendMWH(commandEntity);
        });
    }

    private void sendM(CommandEntity commandEntity) {
        commandEntity.setLastSendTry(System.currentTimeMillis());
        messagingTemplate.convertAndSendToUser("device_" + commandEntity.getOwnMessage().getDeviceId(), "/queue/device", commandEntity.getOwnMessage());
    }

    private void sendMWH(CommandEntity commandEntity) {
        commandEntity.setLastSendTry(System.currentTimeMillis());
        messagingTemplate.convertAndSendToUser("device_" + commandEntity.getOwnMessage().getDeviceId(), "/queue/device", commandEntity.getOwnMessage());
    }

    @Scheduled(fixedDelay = 120_000)
    public void reSendMessage() {
        commandQueue.forEachValue(1, commandEntity -> {
            if (System.currentTimeMillis() - commandEntity.getLastSendTry() >= 120_000)
                sendM(commandEntity);
        });
    }
}
