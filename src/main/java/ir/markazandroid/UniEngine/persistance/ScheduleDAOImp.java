package ir.markazandroid.UniEngine.persistance;

import ir.markazandroid.UniEngine.JSONParser.Parser;
import ir.markazandroid.UniEngine.persistance.entity.ScheduleEntity;
import ir.markazandroid.UniEngine.persistance.entity.ScheduleHasTimeLineEntity;
import ir.markazandroid.UniEngine.persistance.entity.TimeLineEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.ScheduleDAO;
import ir.markazandroid.UniEngine.timelineUtils.Series;
import org.hibernate.Session;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ali on 8/31/2019.
 */
@Repository
public class ScheduleDAOImp implements ScheduleDAO {

    @PersistenceContext
    private EntityManager entityManager;

    private final Parser parser;

    @Autowired
    public ScheduleDAOImp(Parser parser) {
        this.parser = parser;
    }

    @Override
    public void saveSchedule(ScheduleEntity scheduleEntity) {
        entityManager.persist(scheduleEntity);
    }

    @Override
    public void updateSchedule(ScheduleEntity scheduleEntity) {
        entityManager.unwrap(Session.class).update(scheduleEntity);
    }

    @Override
    public void deleteSchedule(ScheduleEntity scheduleEntity) {
        entityManager.remove(scheduleEntity);
    }

    @Override
    public List<ScheduleEntity> getUserSchedules(long userId) {
        return entityManager.createQuery("from ScheduleEntity where userId=:userId" +
                " order by createTime desc ", ScheduleEntity.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public ScheduleEntity getFullSchedule(int scheduleId) {
        ScheduleEntity scheduleEntity = entityManager.unwrap(Session.class)
                .createQuery("from ScheduleEntity sch left outer join fetch sch.scheduleHasTimeLines shts" +
                        " left outer join fetch shts.timeLine where sch.scheduleId=:scheduleId", ScheduleEntity.class)
                .setParameter("scheduleId", scheduleId)
                .uniqueResult();

        if (scheduleEntity != null) {
            scheduleEntity.getScheduleHasTimeLines().parallelStream()
                    .forEach(scheduleHasTimeLineEntity -> scheduleHasTimeLineEntity.getTimeLine().setSeries(
                            new ArrayList<>(parser.get(Series.class,
                                    new JSONArray(scheduleHasTimeLineEntity.getTimeLine().getJsonSeries())))));
        }

        return scheduleEntity;
    }

    @Override
    public ScheduleEntity getScheduleById(int scheduleId) {
        return entityManager.find(ScheduleEntity.class, scheduleId);
    }

    @Override
    public void saveTimeline(TimeLineEntity timeLineEntity) {
        timeLineEntity.setJsonSeries(parser.getArray(timeLineEntity.getSeries()).toString());
        entityManager.persist(timeLineEntity);
    }

    @Override
    public void updateTimeline(TimeLineEntity timeLineEntity) {
        timeLineEntity.setJsonSeries(parser.getArray(timeLineEntity.getSeries()).toString());
        entityManager.unwrap(Session.class).update(timeLineEntity);
    }

    @Override
    public void deleteTimeline(TimeLineEntity timeLineEntity) {
        entityManager.remove(timeLineEntity);
    }

    @Override
    public List<TimeLineEntity> getUserTimelines(long userId) {
        return entityManager.createQuery("from TimeLineEntity where userId=:userId" +
                " order by createTime desc ", TimeLineEntity.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<TimeLineEntity> getUserTimelinesWithSeries(long userId) {
        List<TimeLineEntity> userTimelines = getUserTimelines(userId);
        userTimelines.forEach(this::makeTimelineSeries);
        return userTimelines;
    }

    @Override
    public TimeLineEntity getTimelineById(long timelineId) {
        TimeLineEntity timeline = entityManager.find(TimeLineEntity.class, timelineId);
        makeTimelineSeries(timeline);
        return timeline;

    }

    private void makeTimelineSeries(TimeLineEntity timeline) {
        timeline.setSeries(
                new ArrayList<>(parser.get(Series.class,
                        new JSONArray(timeline.getJsonSeries()))));
    }

    @Override
    public void saveScheduleHasTimeline(ScheduleHasTimeLineEntity scheduleHasTimeLineEntity) {
        entityManager.persist(scheduleHasTimeLineEntity);
    }

    @Override
    public void updateScheduleHasTimeline(ScheduleHasTimeLineEntity scheduleHasTimeLineEntity) {
        entityManager.unwrap(Session.class).update(scheduleHasTimeLineEntity);
    }

    @Override
    public void deleteScheduleHasTimeline(ScheduleHasTimeLineEntity scheduleHasTimeLineEntity) {
        entityManager.remove(scheduleHasTimeLineEntity);
    }

    @Override
    public ScheduleHasTimeLineEntity getScheduleHasTimelineBySHTId(long shtId) {
        return entityManager.find(ScheduleHasTimeLineEntity.class, shtId);
    }

    @Override
    public void detach(Object o) {
        entityManager.detach(o);
    }
}
