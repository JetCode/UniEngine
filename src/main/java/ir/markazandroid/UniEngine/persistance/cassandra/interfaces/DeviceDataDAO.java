package ir.markazandroid.UniEngine.persistance.cassandra.interfaces;

import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceOnline;

import java.util.List;

/**
 * Created by Ali on 6/17/2019.
 */
public interface DeviceDataDAO {

    void saveDeviceOnline(DeviceOnline deviceOnline);

    DeviceOnline getDeviceOnline(long deviceId);

    void saveDeviceData(DeviceData deviceData);

    DeviceData getLatestDeviceData(long deviceId);

    List<DeviceData> getLatestDeviceData(long deviceId, long lateSeconds, String... fields);

    List<DeviceData> getDayFullData(long deviceId, long dayTimestamp, String... fields);
}
