package ir.markazandroid.UniEngine.persistance.cassandra.entity;

import com.datastax.driver.core.LocalDate;
import com.datastax.driver.mapping.annotations.*;
import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Ali on 6/17/2019.
 */
@JSON
@Table(name = "device_data")
public class DeviceData implements Serializable {

    @PartitionKey
    @Column(name = "device_id")
    private long deviceId;

    @PartitionKey(1)
    private LocalDate date;

    @ClusteringColumn
    private long time;

    @Column(name = "values_string")
    private Map<String, String> valuesString;

    @Column(name = "values_int")
    private Map<String, Integer> valuesInt;

    @Column(name = "values_double")
    private Map<String, Double> valuesDouble;

    @JSON
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @JSON(classType = JSON.CLASS_TYPE_TIMESTAMP)
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @JSON
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    //@JSON(classType = JSON.CLASS_TYPE_MAP)
    public Map<String, String> getValuesString() {
        if (valuesString == null) valuesString = new HashMap<>();
        return valuesString;
    }

    public void setValuesString(Map<String, String> valuesString) {
        this.valuesString = valuesString;
    }

    //@JSON(classType = JSON.CLASS_TYPE_MAP)
    public Map<String, Integer> getValuesInt() {
        if (valuesInt == null) valuesInt = new HashMap<>();
        return valuesInt;
    }

    public void setValuesInt(Map<String, Integer> valuesInt) {
        this.valuesInt = valuesInt;
    }

    //@JSON(classType = JSON.CLASS_TYPE_MAP)
    public Map<String, Double> getValuesDouble() {
        if (valuesDouble == null) valuesDouble = new HashMap<>();
        return valuesDouble;
    }

    @Transient
    @JSON(classType = JSON.CLASS_TYPE_MAP)
    public Map<String, Object> getValues() {
        Map<String, Object> map = new HashMap<>();
        map.putAll(getValuesDouble());
        map.putAll(getValuesInt());
        map.putAll(getValuesString());
        return map;
    }

    public void setValues(Map<String, Object> map) {
    }

    public void setValuesDouble(Map<String, Double> valuesDouble) {
        this.valuesDouble = valuesDouble;
    }

    public static String getDate(Date time) {
        return getDate(time.getTime());
    }

    public static String getDate(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return String.format(Locale.US, "%04d-%02d-%02d", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static String getTime(Date time) {
        return getTime(time.getTime());
    }

    public static String getTime(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return String.format(Locale.US, "%02d:%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
    }
}
