package ir.markazandroid.UniEngine.persistance.cassandra;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import ir.markazandroid.UniEngine.conf.cassandra.Connector;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceOnline;
import ir.markazandroid.UniEngine.persistance.cassandra.interfaces.DeviceDataDAO;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by Ali on 6/17/2019.
 */
@Repository
public class DeviceDataDAOImp implements DeviceDataDAO {

    private final Connector connector;
    private final Mapper<DeviceData> dataMapper;
    private final Mapper<DeviceOnline> onlineMapper;
    private final MappingManager manager;

    public DeviceDataDAOImp(Connector connector) {
        this.connector = connector;
        manager = new MappingManager(connector.getSession());
        dataMapper = manager.mapper(DeviceData.class);
        onlineMapper = manager.mapper(DeviceOnline.class);
    }

    @Override
    public void saveDeviceOnline(DeviceOnline deviceOnline) {
        onlineMapper.save(deviceOnline);
    }

    @Override
    public DeviceOnline getDeviceOnline(long deviceId) {
        return onlineMapper.map(manager.getSession().execute(String.format("select * from device_online " +
                "where device_id=%d", deviceId)))
                .one();
    }

    @Override
    public void saveDeviceData(DeviceData deviceData) {
        dataMapper.save(deviceData);
    }

    @Override
    public DeviceData getLatestDeviceData(long deviceId) {
        return dataMapper.map(dataMapper.getManager().getSession().execute(String.format("select * from device_data " +
                "where device_id=%d and date='%s' limit 1", deviceId, getTodayDate())))
                .one();
    }

    @Override
    public List<DeviceData> getLatestDeviceData(long deviceId, long lateSeconds, String... fields) {
        return dataMapper.map(dataMapper.getManager().getSession().execute(String.format("select %s from device_data " +
                        "where device_id=%d and date='%s' and time >= '%s'",
                formatProjections(fields), deviceId, getTodayDate(), getLateTime(lateSeconds))))
                .all();
    }

    @Override
    public List<DeviceData> getDayFullData(long deviceId, long dayTimestamp, String... fields) {
        return dataMapper.map(dataMapper.getManager().getSession().execute(String.format("select %s from device_data " +
                        "where device_id=%d and date='%s'",
                formatProjections(fields), deviceId, DeviceData.getDate(dayTimestamp))))
                .all();
    }

    private static String getTodayDate() {
        return DeviceData.getDate(System.currentTimeMillis());
    }

    private static String getTime() {
        return getTime(System.currentTimeMillis());
    }

    private static String getTime(long timeStamp) {
        return DeviceData.getTime(timeStamp);
    }

    private static String getLateTime(long seconds) {
        return DeviceData.getTime(System.currentTimeMillis() - (seconds * 1000));
    }

    private static String formatProjections(String[] projections) {
        if (projections.length == 0) return "*";
        return StringUtils.arrayToCommaDelimitedString(projections);
    }
}
