package ir.markazandroid.UniEngine.persistance.cassandra.entity;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Ali on 9/21/2019.
 */
@JSON
@Table(name = "device_online")
public class DeviceOnline implements Serializable {

    @PartitionKey
    @Column(name = "device_id")
    private long deviceId;

    @Column(name = "time")
    private Date time;

    public DeviceOnline() {
    }

    public DeviceOnline(long deviceId, Date time) {
        this.deviceId = deviceId;
        this.time = time;
    }

    public DeviceOnline(long deviceId, long time) {
        this(deviceId, new Date(time));
    }

    @JSON
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }


    @JSON(classType = JSON.CLASS_TYPE_TIMESTAMP)
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceOnline that = (DeviceOnline) o;
        return Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, time);
    }
}
