package ir.markazandroid.UniEngine.persistance.file.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Ali on 10/13/2019.
 */
@Entity
@Table(name = "files", schema = "metadata")
public class MetaDataEntity implements Serializable {

    private String fileId;
    private String reference;

    public MetaDataEntity(String fileId, String reference) {
        this.fileId = fileId;
        this.reference = reference;
    }

    public MetaDataEntity() {
    }

    @Id
    @Column(name = "file_id")
    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    @Id
    @Column(name = "reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaDataEntity that = (MetaDataEntity) o;
        return Objects.equals(fileId, that.fileId) &&
                Objects.equals(reference, that.reference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileId, reference);
    }
}
