package ir.markazandroid.UniEngine.persistance;

import ir.markazandroid.UniEngine.persistance.entity.CommandEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.CommandDAO;
import ir.markazandroid.UniEngine.util.DaoUtils;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Ali on 2/6/2019.
 */
@Repository
public class CommandDAOImp implements CommandDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveCommand(CommandEntity commandEntity) {
        entityManager.persist(commandEntity);
    }

    @Override
    public void updateCommand(CommandEntity commandEntity, String... fields) {
        if (fields.length > 0) {
            CriteriaUpdate<CommandEntity> criteriaUpdate = entityManager.getCriteriaBuilder()
                    .createCriteriaUpdate(CommandEntity.class);
            Root<CommandEntity> root = criteriaUpdate.from(CommandEntity.class);
            DaoUtils.setUpdateFields(commandEntity, criteriaUpdate, fields);
            criteriaUpdate.where(entityManager.getCriteriaBuilder()
                    .equal(root.get("commandId"), commandEntity.getCommandId()));

            entityManager.createQuery(criteriaUpdate)
                    .executeUpdate();
        } else {
            Session session = entityManager.unwrap(Session.class);
            session.update(commandEntity);
        }
    }

    @Override
    public List<CommandEntity> getDeviceCommands(long deviceId) {
        return entityManager.createQuery("from CommandEntity where deviceId=:deviceId", CommandEntity.class)
                .setParameter("deviceId", deviceId)
                .getResultList();
    }

    @Override
    public List<CommandEntity> getDeviceUnReceivedCommands(long deviceId) {
        return entityManager.createQuery("from CommandEntity where deviceId=:deviceId" +
                " and status<0", CommandEntity.class)
                .setParameter("deviceId", deviceId)
                .getResultList();
    }

    @Override
    public List<CommandEntity> getUnReceivedCommands() {
        return entityManager.createQuery("from CommandEntity where" +
                " status<0", CommandEntity.class)
                .getResultList();
    }


    @Override
    public CommandEntity getCommandByMessageId(String messageId) {
        return entityManager.unwrap(Session.class)
                .createQuery("from CommandEntity where messageId=:messageId", CommandEntity.class)
                .setParameter("messageId", messageId)
                .uniqueResult();
    }
}
