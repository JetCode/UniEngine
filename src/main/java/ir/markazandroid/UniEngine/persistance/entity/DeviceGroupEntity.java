package ir.markazandroid.UniEngine.persistance.entity;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Ali on 7/17/2019.
 */
@JSON
@Entity
@Table(name = "device_group", schema = "uni_engine")
public class DeviceGroupEntity implements Serializable {
    private long deviceGroupId;
    private long userId;
    private String groupName;

    private Set<DeviceEntity> devices;

    @JSON
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_group_id", nullable = false)
    public long getDeviceGroupId() {
        return deviceGroupId;
    }

    public void setDeviceGroupId(long deviceGroupId) {
        this.deviceGroupId = deviceGroupId;
    }

    @JSON
    @Column(name = "user_id", nullable = false)
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @JSON
    @Basic
    @Column(name = "group_name", nullable = true, length = -1)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceGroupEntity that = (DeviceGroupEntity) o;
        return deviceGroupId == that.deviceGroupId &&
                userId == that.userId &&
                Objects.equals(groupName, that.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceGroupId, userId, groupName);
    }


    @JSON(classType = JSON.CLASS_TYPE_ARRAY, clazz = DeviceEntity.class, collectionClass = HashSet.class)
    @ManyToMany
    @JoinTable(name = "device_group_has_device",
            joinColumns = {@JoinColumn(name = "device_group_id")},
            inverseJoinColumns = {@JoinColumn(name = "device_id")})
    public Set<DeviceEntity> getDevices() {
        return devices;
    }

    public void setDevices(Set<DeviceEntity> deviceEntities) {
        this.devices = deviceEntities;
    }
}
