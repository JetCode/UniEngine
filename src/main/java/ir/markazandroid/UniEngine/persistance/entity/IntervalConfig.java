package ir.markazandroid.UniEngine.persistance.entity;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;

/**
 * Created by Ali on 9/4/2019.
 */
@JSON
public class IntervalConfig implements Serializable {

    private int interval;
    private long deviceId;

    @JSON
    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    @JSON
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }
}
