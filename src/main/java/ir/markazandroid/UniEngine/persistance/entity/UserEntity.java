package ir.markazandroid.UniEngine.persistance.entity;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.conf.session.MainPrincipalObject;
import ir.markazandroid.UniEngine.object.User;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

/**
 * Created by Ali on 30/01/2018.
 */
@JSON
@Entity
@Table(name = "user", schema = "uni_engine")
public class UserEntity extends User implements Serializable, MainPrincipalObject {

    public static final int STATUS_DISABLED=-2;
    public static final int STATUS_NOT_VERIFIED=-1;
    public static final int STATUS_VERIFIED_HAS_NO_DEVICE = 1;
    public static final int STATUS_VERIFIED_HAS_DEVICE = 2;


    public static final String USER_TYPE_NORMAL = "normal";
    public static final String USER_TYPE_COMPANY = "company";


    private long userId;
    private String passwordRetype;
    private Timestamp createTime;
    private int status;
    private String phone;
    private String email;
    private String firstName;
    private String lastName;
    private Integer roleId;
    private RoleEntity role;

    //Transients
    private Params params;
    private Collection<DeviceEntity> devices;
    private String password;
    private String type;
    private String meliCode;
    private String companyName;

    public static String buildUniqueKey(long userId) {
        return UserEntity.class.getSimpleName() + "_" + userId;
    }

    @JSON
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 50)
    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JSON(classType = JSON.CLASS_TYPE_TIMESTAMP)
    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return userId == that.userId &&
                Objects.equals(getUsername(), that.getUsername()) &&
                Objects.equals(password, that.password) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, getUsername(), password, createTime);
    }

    @Transient
    public String getPasswordRetype() {
        return passwordRetype;
    }

    public void setPasswordRetype(String passwordRetype) {
        this.passwordRetype = passwordRetype;
    }

    @JSON
    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @JSON
    @Basic
    @Column(name = "phone", nullable = true, length = 13)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JSON
    @Basic
    @Column(name = "email", nullable = true, length = 200)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JSON
    @Basic
    @Column(name = "first_name", nullable = true, length = -1)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JSON
    @Basic
    @Column(name = "last_name", length = -1)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "role_id")
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", referencedColumnName = "role_id",insertable = false,updatable = false)
    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    @Transient
    @Override
    public String getStoragePrefix() {
        return userId + "_" + getUsername();
    }

    @Transient
    @Override
    public long getMaxCapacity() {
        //5 GB
        return 5*1024*1024*1024L;
    }

    @Transient
    @Override
    public String getUniqueKey() {
        return buildUniqueKey(userId);
    }

    @Transient
    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    @OneToMany(mappedBy = "user")
    public Collection<DeviceEntity> getDevices() {
        return devices;
    }

    public void setDevices(Collection<DeviceEntity> devices) {
        this.devices = devices;
    }

    @JSON
    @Basic
    @Column(name = "type", nullable = false, length = 20)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "meli_code", nullable = true, length = -1)
    public String getMeliCode() {
        return meliCode;
    }

    public void setMeliCode(String meliCode) {
        this.meliCode = meliCode;
    }

    @JSON
    @Basic
    @Column(name = "company_name", nullable = true, length = -1)
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @JSON
    public static class Params {
        private int deviceCount;

        @JSON
        public int getDeviceCount() {
            return deviceCount;
        }

        public void setDeviceCount(int deviceCount) {
            this.deviceCount = deviceCount;
        }
    }


    @Transient
    @Override
    public String getUsername() {
        return getEmail();
    }
}
