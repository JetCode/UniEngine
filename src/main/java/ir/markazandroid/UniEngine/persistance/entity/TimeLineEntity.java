package ir.markazandroid.UniEngine.persistance.entity;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.timelineUtils.Series;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Created by Ali on 4/8/2019.
 */
@JSON
@Entity
@Table(name = "time_line", schema = "uni_engine", catalog = "")
public class TimeLineEntity implements Serializable {
    private long timelineId;
    private Long userId;
    private String jsonSeries;
    private Timestamp createTime;
    private String name;
    private Collection<ScheduleHasTimeLineEntity> shts;

    //Transients
    private ArrayList<Series> series;


    @JSON
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "time_line_id", nullable = false)
    public long getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(long timeLineId) {
        this.timelineId = timeLineId;
    }

    @JSON
    @Basic
    @Column(name = "user_id", nullable = true)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeLineEntity that = (TimeLineEntity) o;
        return timelineId == that.timelineId &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timelineId, userId);
    }

    @Basic
    @Column(name = "json_series", nullable = false, length = -1)
    public String getJsonSeries() {
        return jsonSeries;
    }

    public void setJsonSeries(String jsonSeries) {
        this.jsonSeries = jsonSeries;
    }

    @Transient
    @JSON(classType = JSON.CLASS_TYPE_ARRAY, clazz = Series.class)
    public ArrayList<Series> getSeries() {
        return series;
    }

    public void setSeries(ArrayList<Series> series) {
        this.series = series;
    }

    @JSON(classType = JSON.CLASS_TYPE_TIMESTAMP)
    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @JSON
    @Basic
    @Column(name = "name", nullable = true, length = -1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "timeLine")
    public Collection<ScheduleHasTimeLineEntity> getShts() {
        return shts;
    }

    public void setShts(Collection<ScheduleHasTimeLineEntity> shts) {
        this.shts = shts;
    }
}
