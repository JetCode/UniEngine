package ir.markazandroid.UniEngine.persistance.entity;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Created by Ali on 4/8/2019.
 */
@JSON
@Entity
@Table(name = "schedule", schema = "uni_engine")
public class ScheduleEntity implements Serializable {
    private int scheduleId;
    private Byte type;
    private Long userId;
    private Collection<ScheduleHasTimeLineEntity> scheduleHasTimeLines;
    private String name;
    private Timestamp createTime;
    private List<DeviceEntity> devices;

    @JSON
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id", nullable = false)
    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    @JSON
    @Basic
    @Column(name = "type", nullable = true)
    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    @JSON
    @Basic
    @Column(name = "user_id", nullable = true)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduleEntity that = (ScheduleEntity) o;
        return scheduleId == that.scheduleId &&
                Objects.equals(type, that.type) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheduleId, type, userId);
    }

    @JSON(classType = JSON.CLASS_TYPE_ARRAY, clazz = ScheduleHasTimeLineEntity.class)
    @OneToMany(mappedBy = "schedule")
    public Collection<ScheduleHasTimeLineEntity> getScheduleHasTimeLines() {
        return scheduleHasTimeLines;
    }

    public void setScheduleHasTimeLines(Collection<ScheduleHasTimeLineEntity> scheduleHasTimeLines) {
        this.scheduleHasTimeLines = scheduleHasTimeLines;
    }

    @JSON
    @Basic
    @Column(name = "name", nullable = true, length = -1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JSON(classType = JSON.CLASS_TYPE_TIMESTAMP)
    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @OneToMany(mappedBy = "advertiserSchedule")
    public List<DeviceEntity> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceEntity> devices) {
        this.devices = devices;
    }
}
