package ir.markazandroid.UniEngine.persistance.entity;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Ali on 10/30/2019.
 */
@JSON
@Entity
@Table(name = "device_model", schema = "uni_engine", catalog = "")
public class DeviceModelEntity implements Serializable {
    private int deviceModelId;
    private String name;
    private String details;
    private String configUrl;

    @JSON
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_model_id", nullable = false)
    public int getDeviceModelId() {
        return deviceModelId;
    }

    public void setDeviceModelId(int deviceModelId) {
        this.deviceModelId = deviceModelId;
    }

    @JSON
    @Basic
    @Column(name = "name", nullable = false, length = -1)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JSON
    @Basic
    @Column(name = "details", nullable = true, length = -1)
    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceModelEntity that = (DeviceModelEntity) o;
        return deviceModelId == that.deviceModelId &&
                Objects.equals(name, that.name) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceModelId, name, details);
    }

    @JSON
    @Basic
    @Column(name = "config_url", nullable = true, length = -1)
    public String getConfigUrl() {
        return configUrl;
    }

    public void setConfigUrl(String configUrl) {
        this.configUrl = configUrl;
    }
}
