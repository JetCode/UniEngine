package ir.markazandroid.UniEngine.persistance.entity;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Ali on 4/8/2019.
 */
@JSON
@Entity
@Table(name = "schedule_has_time_line", schema = "uni_engine", catalog = "")
public class ScheduleHasTimeLineEntity implements Serializable {
    private int scheduleId;
    private long timelineId;
    private byte croned;
    private String cron;
    private ScheduleEntity schedule;
    private TimeLineEntity timeLine;
    private long shtId;
    private String dates;

    @JSON
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sht_id", nullable = false)
    public long getShtId() {
        return shtId;
    }

    public void setShtId(long shtId) {
        this.shtId = shtId;
    }

    @JSON
    @Column(name = "schedule_id", nullable = false)
    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    @JSON
    @Column(name = "time_line_id", nullable = false)
    public long getTimelineId() {
        return timelineId;
    }

    public void setTimelineId(long timeLineId) {
        this.timelineId = timeLineId;
    }

    @JSON(classType = JSON.CLASS_TYPE_BOOLEAN)
    @Basic
    @Column(name = "croned", nullable = false)
    public byte getCroned() {
        return croned;
    }

    public void setCroned(byte croned) {
        this.croned = croned;
    }

    @JSON
    @Basic
    @Column(name = "cron", nullable = true, length = -1)
    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduleHasTimeLineEntity that = (ScheduleHasTimeLineEntity) o;
        return scheduleId == that.scheduleId &&
                timelineId == that.timelineId &&
                croned == that.croned &&
                Objects.equals(cron, that.cron);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scheduleId, timelineId, croned, cron);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id", nullable = false,insertable = false,updatable = false)
    public ScheduleEntity getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleEntity schedule) {
        this.schedule = schedule;
    }

    @JSON(classType = JSON.CLASS_TYPE_OBJECT, clazz = TimeLineEntity.class)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "time_line_id", referencedColumnName = "time_line_id", nullable = false, insertable = false, updatable = false)
    public TimeLineEntity getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(TimeLineEntity timeLine) {
        this.timeLine = timeLine;
    }

    @JSON
    @Basic
    @Column(name = "dates", nullable = true, length = -1)
    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }
}
