package ir.markazandroid.UniEngine.persistance.entity;


import ir.markazandroid.UniEngine.object.WebSocketMessage;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Created by Ali on 2/6/2019.
 */
@Entity
@Table(name = "command", schema = "uni_engine")
public class CommandEntity implements Serializable {


    public static final byte STATUS_NOT_RECEIVED = -1;
    public static final byte STATUS_CANCELED = 0;
    public static final byte STATUS_RECEIVED = 1;
    public static final byte STATUS_SUCCESS = 2;
    public static final byte STATUS_ERROR = 3;

    private long deviceId;
    private long commandId;
    private String command;
    private String result;
    private byte status;
    private Timestamp createTime;
    private Timestamp receiveTime;
    private DeviceEntity device;
    private String messageId;
    private String ownMessageJson;

    //Transients
    private WebSocketMessage ownMessage;
    private long lastSendTry;
    private Integer cmdCode;

    @Column(name = "device_id", nullable = false)
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "command_id", nullable = false)
    public long getCommandId() {
        return commandId;
    }

    public void setCommandId(long commandId) {
        this.commandId = commandId;
    }

    @Basic
    @Column(name = "command", nullable = false, length = -1)
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Basic
    @Column(name = "result", nullable = true, length = -1)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    @Basic
    @Column(name = "create_time", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "receive_time", nullable = true)
    public Timestamp getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Timestamp receiveTime) {
        this.receiveTime = receiveTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommandEntity that = (CommandEntity) o;
        return deviceId == that.deviceId &&
                commandId == that.commandId &&
                status == that.status &&
                Objects.equals(command, that.command) &&
                Objects.equals(result, that.result) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(receiveTime, that.receiveTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, commandId, command, result, status, createTime, receiveTime);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id", referencedColumnName = "device_id", nullable = false, insertable = false, updatable = false)
    public DeviceEntity getDevice() {
        return device;
    }

    public void setDevice(DeviceEntity device) {
        this.device = device;
    }

    @Basic
    @Column(name = "message_id", nullable = false, length = 100)
    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @Transient
    public WebSocketMessage getOwnMessage() {
        return ownMessage;
    }

    public void setOwnMessage(WebSocketMessage ownMessage) {
        this.ownMessage = ownMessage;
    }

    @Basic
    @Column(name = "own_message_json", nullable = true, length = -1)
    public String getOwnMessageJson() {
        return ownMessageJson;
    }

    public void setOwnMessageJson(String ownMessageJson) {
        this.ownMessageJson = ownMessageJson;
    }

    @Transient
    public long getLastSendTry() {
        return lastSendTry;
    }

    public void setLastSendTry(long lastSendTry) {
        this.lastSendTry = lastSendTry;
    }

    @Basic
    @Column(name = "cmd_code", nullable = true)
    public Integer getCmdCode() {
        return cmdCode;
    }

    public void setCmdCode(Integer cmdCode) {
        this.cmdCode = cmdCode;
    }
}
