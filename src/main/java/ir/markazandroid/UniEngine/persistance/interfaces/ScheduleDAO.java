package ir.markazandroid.UniEngine.persistance.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.ScheduleEntity;
import ir.markazandroid.UniEngine.persistance.entity.ScheduleHasTimeLineEntity;
import ir.markazandroid.UniEngine.persistance.entity.TimeLineEntity;

import java.util.List;

/**
 * Created by Ali on 8/31/2019.
 */
public interface ScheduleDAO {

    void saveSchedule(ScheduleEntity scheduleEntity);

    void updateSchedule(ScheduleEntity scheduleEntity);

    void deleteSchedule(ScheduleEntity scheduleEntity);

    List<ScheduleEntity> getUserSchedules(long userId);

    ScheduleEntity getFullSchedule(int scheduleId);

    ScheduleEntity getScheduleById(int scheduleId);

    void saveTimeline(TimeLineEntity timeLineEntity);

    void updateTimeline(TimeLineEntity timeLineEntity);

    void deleteTimeline(TimeLineEntity timeLineEntity);

    List<TimeLineEntity> getUserTimelines(long userId);

    List<TimeLineEntity> getUserTimelinesWithSeries(long userId);

    TimeLineEntity getTimelineById(long timelineId);

    void saveScheduleHasTimeline(ScheduleHasTimeLineEntity scheduleHasTimeLineEntity);

    void updateScheduleHasTimeline(ScheduleHasTimeLineEntity scheduleHasTimeLineEntity);

    void deleteScheduleHasTimeline(ScheduleHasTimeLineEntity scheduleHasTimeLineEntity);

    ScheduleHasTimeLineEntity getScheduleHasTimelineBySHTId(long shtId);


    void detach(Object o);
}
