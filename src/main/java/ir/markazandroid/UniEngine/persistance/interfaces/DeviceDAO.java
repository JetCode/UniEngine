package ir.markazandroid.UniEngine.persistance.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceGroupEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceModelEntity;

import java.util.List;

/**
 * Created by Ali on 6/9/2019.
 */
public interface DeviceDAO {

    void saveDevice(DeviceEntity deviceEntity);

    void updateDevice(DeviceEntity deviceEntity, String... fields);

    DeviceEntity getDeviceById(long deviceId, String... fields);

    DeviceEntity getDeviceByName(String name);

    DeviceEntity getDeviceByUUID(String UUID);

    List<DeviceEntity> getUserDevices(long userId);

    List<DeviceEntity> getNotAssignedDevicesByOwnerCode(String ownerCode);

    void saveDeviceGroup(DeviceGroupEntity deviceGroupEntity);

    void updateDeviceGroup(DeviceGroupEntity deviceGroupEntity);

    DeviceGroupEntity getDeviceGroupById(long deviceGroupId);

    List<DeviceGroupEntity> getUserDeviceGroups(long userId);

    void deleteDeviceGroup(long deviceGroupId);

    List<DeviceModelEntity> getDeviceModels();





}
