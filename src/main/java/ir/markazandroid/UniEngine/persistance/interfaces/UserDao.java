package ir.markazandroid.UniEngine.persistance.interfaces;


import ir.markazandroid.UniEngine.persistance.entity.UserEntity;

import java.util.List;

/**
 * Created by Ali on 30/01/2018.
 */
public interface UserDao {

    UserEntity getUserByEmail(String email, String... fields);

    UserEntity getUserByUserId(long userId, String... fields);

    UserEntity getUserByMeliCode(String meliCode, String... fields);


    // UserEntity getUserByPhone(String phone, String... fields);

    void saveUser(UserEntity userEntity);

    void updateUser(UserEntity userEntity, String... fields);

    void mergeUser(UserEntity userEntity);

    List<UserEntity> getUsers(int offset, int limit);

    long getUsersCount();

    void detachObject(Object o);

}
