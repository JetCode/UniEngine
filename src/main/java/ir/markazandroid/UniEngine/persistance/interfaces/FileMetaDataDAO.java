package ir.markazandroid.UniEngine.persistance.interfaces;

import ir.markazandroid.UniEngine.persistance.file.entity.MetaDataEntity;

import java.util.List;

/**
 * Created by Ali on 10/13/2019.
 */
public interface FileMetaDataDAO {

    List<MetaDataEntity> getFileMetaDatas(String fileId);

    void saveMetaData(MetaDataEntity metaDataEntity);

    void deleteMetaData(MetaDataEntity metaDataEntity);

    void deleteFile(String fileId);
}
