package ir.markazandroid.UniEngine.persistance.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;
import ir.markazandroid.UniEngine.persistance.entity.LayoutEntity;
import ir.markazandroid.UniEngine.persistance.entity.PlayListEntity;
import org.springframework.lang.Nullable;

import java.util.List;

/**
 * Created by Ali on 4/14/2019.
 */
public interface CampaignDAO {

    void savePlayList(PlayListEntity playListEntity);
    void updatePlayList(PlayListEntity playListEntity);

    void deletePlayList(long playListId);

    List<PlayListEntity> getUserPlayLists(long userId);

    PlayListEntity getPlayListById(long playListId);


    void saveCampaign(CampaignEntity campaignEntity);
    void updateCampaign(CampaignEntity campaignEntity);

    void deleteCampaign(long campaignId);

    List<CampaignEntity> getUserCampaigns(long userId);

    CampaignEntity getCampaignById(long campaignId);

    /**
     * Cached value high performance
     *
     * @param userId
     * @return
     */
    long getUserCampaignCount(@Nullable long userId);


    LayoutEntity getLayoutById(long layoutId);

    void saveLayout(LayoutEntity layoutEntity);


    List<LayoutEntity> getUserLayouts(long userId, String orientation);

    List<LayoutEntity> getUserLayouts(long userId);

    List<LayoutEntity> getGlobalLayouts(String orientation);

    void deleteLayout(long layoutId);


    void detach(Object o);


}
