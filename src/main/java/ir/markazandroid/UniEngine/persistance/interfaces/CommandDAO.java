package ir.markazandroid.UniEngine.persistance.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.CommandEntity;

import java.util.List;

/**
 * Created by Ali on 2/6/2019.
 */
public interface CommandDAO {

    void saveCommand(CommandEntity commandEntity);

    void updateCommand(CommandEntity commandEntity, String... fields);

    List<CommandEntity> getDeviceCommands(long deviceId);

    List<CommandEntity> getDeviceUnReceivedCommands(long deviceId);

    List<CommandEntity> getUnReceivedCommands();

    CommandEntity getCommandByMessageId(String messageId);

}
