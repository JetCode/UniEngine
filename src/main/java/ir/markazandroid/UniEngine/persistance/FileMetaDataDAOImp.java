package ir.markazandroid.UniEngine.persistance;

import ir.markazandroid.UniEngine.persistance.file.entity.MetaDataEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.FileMetaDataDAO;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Ali on 10/13/2019.
 */
@Repository
public class FileMetaDataDAOImp implements FileMetaDataDAO {

    @PersistenceContext(unitName = "fileMetaData")
    private EntityManager entityManager;

    @Override
    public List<MetaDataEntity> getFileMetaDatas(String fileId) {
        return entityManager.createQuery("from MetaDataEntity where fileId=:fileId", MetaDataEntity.class)
                .setParameter("fileId", fileId)
                .getResultList();
    }

    @Transactional("sqlite")
    @Override
    public void saveMetaData(MetaDataEntity metaDataEntity) {
        entityManager.unwrap(Session.class).saveOrUpdate(metaDataEntity);
    }

    @Transactional("sqlite")
    @Override
    public void deleteMetaData(MetaDataEntity metaDataEntity) {
        entityManager.remove(entityManager.find(MetaDataEntity.class, metaDataEntity));
    }

    @Transactional("sqlite")
    @Override
    public void deleteFile(String fileId) {
        entityManager.createQuery("delete from MetaDataEntity where fileId=:fileId")
                .setParameter("fileId", fileId)
                .executeUpdate();
    }
}
