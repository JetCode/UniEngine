package ir.markazandroid.UniEngine.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Created by ali on 14/08/2017.
 * sends mail
 */
@Component
public class EmailUtils {

    private final JavaMailSender emailSender;

    @Autowired
    public EmailUtils(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    /**
     * simply sends mail
     * @param to
     * @param subject
     * @param text
     */
    @Async
    public void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            emailSender.send(message);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
