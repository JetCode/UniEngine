package ir.markazandroid.UniEngine.security.api;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ali on 11/12/2017.
 */
@Component
public class RestLogoutSuccessHandler implements LogoutSuccessHandler {


    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        authentication.setAuthenticated(false);
        SecurityContextHolder.getContext().setAuthentication(null);
        SecurityContextHolder.clearContext();
        response.setStatus(200);
        response.addHeader("Content-Type", "application/json");
        response.getWriter().print("{\"response\":\"Success\"}");
        //response.getHeaderNames().remove(HttpHeaders.LOCATION);
    }
}
