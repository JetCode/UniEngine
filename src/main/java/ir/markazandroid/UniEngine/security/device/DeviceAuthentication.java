package ir.markazandroid.UniEngine.security.device;

import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by Ali on 5/28/2018.
 */
class DeviceAuthentication implements Authentication {

    private final DeviceEntity deviceEntity;
    private boolean authenticated;

    public DeviceAuthentication(DeviceEntity deviceEntity) {
        this.deviceEntity = deviceEntity;
        authenticated = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return deviceEntity.getAuthorities();
    }

    @Override
    public Object getCredentials() {
        return deviceEntity.getPassword();
    }

    @Override
    public Object getDetails() {
        return deviceEntity;
    }

    @Override
    public Object getPrincipal() {
        return deviceEntity;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        authenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return deviceEntity.getName();
    }
}
