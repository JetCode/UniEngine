package ir.markazandroid.UniEngine.security.userApi;

import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by Ali on 5/28/2018.
 */
class UserAuthentication implements Authentication {

    private final UserEntity userEntity;
    private boolean authenticated;

    public UserAuthentication(UserEntity userEntity) {
        this.userEntity = userEntity;
        authenticated = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userEntity.getAuthorities();
    }

    @Override
    public Object getCredentials() {
        return userEntity.getPassword();
    }

    @Override
    public Object getDetails() {
        return userEntity;
    }

    @Override
    public Object getPrincipal() {
        return userEntity;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        authenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return userEntity.getFirstName() + " " + userEntity.getLastName();
    }
}
