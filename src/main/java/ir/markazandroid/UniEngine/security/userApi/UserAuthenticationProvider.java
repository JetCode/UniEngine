package ir.markazandroid.UniEngine.security.userApi;

import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.service.interfaces.UserService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by Ali on 5/28/2018.
 */
public class UserAuthenticationProvider implements AuthenticationProvider {

    private final UserService userService;

    public UserAuthenticationProvider(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UserEntity userEntity = userService.getUserByEmail(authentication.getPrincipal().toString());

        if (userEntity == null || !userEntity.getPassword().equals(authentication.getCredentials()))
            throw new UsernameNotFoundException(authentication.getPrincipal().toString());

        return new UserAuthentication(userService.getUserWithPrivileges(userEntity.getUserId()));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.equals(authentication);
    }
}
