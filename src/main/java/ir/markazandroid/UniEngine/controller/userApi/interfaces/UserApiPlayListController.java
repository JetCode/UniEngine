package ir.markazandroid.UniEngine.controller.userApi.interfaces;

import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.persistance.entity.PlayListEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Ali on 4/13/2019.
 */
@RequestMapping(value = "/userApi/playlist", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface UserApiPlayListController {

    @GetMapping("/list")
    List<PlayListEntity> getPlayLists(UserEntity userEntity, HttpServletRequest request);

    @PostMapping(value = "/saveOrUpdate", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseObject> saveOrUpdatePlayList(UserEntity userEntity, @RequestBody PlayListEntity playListEntity);

    @PostMapping(value = "/delete")
    ResponseEntity deletePlayList(UserEntity userEntity, @RequestParam long playlistId);


}
