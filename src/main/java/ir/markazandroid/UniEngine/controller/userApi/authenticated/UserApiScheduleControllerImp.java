package ir.markazandroid.UniEngine.controller.userApi.authenticated;

import ir.markazandroid.UniEngine.JSONParser.JsonProfile;
import ir.markazandroid.UniEngine.controller.userApi.interfaces.UserApiScheduleController;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.persistance.entity.*;
import ir.markazandroid.UniEngine.service.interfaces.CampaignService;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import ir.markazandroid.UniEngine.service.interfaces.ScheduleService;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ali on 9/1/2019.
 */
@RestController
public class UserApiScheduleControllerImp implements UserApiScheduleController {

    private final ScheduleService scheduleService;
    private final CampaignService campaignService;
    private final DeviceService deviceService;

    public UserApiScheduleControllerImp(ScheduleService scheduleService, CampaignService campaignService, DeviceService deviceService) {
        this.scheduleService = scheduleService;
        this.campaignService = campaignService;
        this.deviceService = deviceService;
    }

    @Override
    public JsonProfile getSchedules(UserEntity userEntity) {
        return new JsonProfile(scheduleService.getUserSchedules(userEntity.getUserId()))
                .excludeFields("scheduleHasTimeLines");
    }

    @Override
    public ScheduleEntity getScheduleDetails(UserEntity userEntity, int scheduleId) {
        ScheduleEntity scheduleEntity = scheduleService.getDetailedSchedule(scheduleId);
        if (scheduleEntity == null || scheduleEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        return scheduleEntity;
    }

    @Override
    public ScheduleEntity createSchedule(UserEntity userEntity, String name) {
        ScheduleEntity scheduleEntity = new ScheduleEntity();
        scheduleEntity.setUserId(userEntity.getUserId());
        scheduleEntity.setName(StringUtils.trimWhitespace(name));
        return scheduleService.newSchedule(scheduleEntity);
    }

    @Override
    public ResponseEntity updateSchedule(UserEntity userEntity, int scheduleId, String name) {
        ScheduleEntity scheduleEntity = scheduleService.getScheduleById(scheduleId);
        if (scheduleEntity == null || scheduleEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        scheduleService.updateScheduleName(scheduleId, StringUtils.trimWhitespace(name));
        return new ResponseObject.Builder().status(200).message(String.format("Schedule name updated Successfully.")).timestamp(System.currentTimeMillis()).build();
    }

    @Override
    public ResponseEntity deleteSchedule(UserEntity userEntity, int scheduleId) {
        ScheduleEntity scheduleEntity = scheduleService.getScheduleById(scheduleId);
        if (scheduleEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        scheduleService.deleteSchedule(scheduleId);

        return new ResponseObject.Builder().status(200).message("Schedule deleted Successfully.").timestamp(System.currentTimeMillis()).build();
    }

    @Override
    public ResponseEntity assignSchedule(UserEntity userEntity, int scheduleId, long deviceId) {
        ScheduleEntity scheduleEntity = scheduleService.getScheduleById(scheduleId);
        DeviceEntity deviceEntity = deviceService.getDeviceById(deviceId);
        if (scheduleEntity == null || deviceEntity == null
                || scheduleEntity.getUserId() != userEntity.getUserId()
                || deviceEntity.getUserId() == null || deviceEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        deviceService.refreshActiveAdvertisingSchedule(deviceId, scheduleId);

        return new ResponseObject.Builder().status(200).message("Assign Successful").timestamp(System.currentTimeMillis()).build();
    }

    @Override
    public ResponseEntity unAssignSchedule(UserEntity userEntity, long deviceId) {
        DeviceEntity deviceEntity = deviceService.getDeviceById(deviceId);
        if (deviceEntity == null || deviceEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        deviceService.refreshActiveAdvertisingSchedule(deviceId, null);

        return new ResponseObject.Builder().status(200).message("UnAssign Successful").timestamp(System.currentTimeMillis()).build();

    }

    @Override
    public List<DeviceEntity> getAssignedDevices(UserEntity userEntity, int scheduleId) {
        ScheduleEntity scheduleEntity = scheduleService.getScheduleById(scheduleId);
        if (scheduleEntity == null || scheduleEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        List<DeviceEntity> deviceEntities = scheduleEntity.getDevices();

        return new ArrayList<>(deviceEntities);
    }

    //TODO Check input
    @Override
    public ResponseEntity saveOrUpdateTimeline(UserEntity userEntity, TimeLineEntity timeLine) {
        boolean isUpdate = timeLine.getTimelineId() != 0;

        if (isUpdate) {
            TimeLineEntity timeLineEntity = scheduleService.getTimelineById(timeLine.getTimelineId());
            if (timeLineEntity == null || timeLineEntity.getUserId() != userEntity.getUserId())
                throw new NotFoundException();
        } else
            timeLine.setUserId(userEntity.getUserId());

        timeLine.getSeries().forEach(series -> series.getCampaigns().forEach(seriesCampaign -> {
            CampaignEntity campaignEntity = campaignService.getCampaignById(seriesCampaign.getCampaignId());
            if (campaignEntity == null)
                throw new NotFoundException(String.format("Campaign Id %d not found!", seriesCampaign.getCampaignId()));
            seriesCampaign.setCampaignName(campaignEntity.getName());
        }));

        scheduleService.saveOrUpdateTimeline(timeLine);

        return new ResponseObject.Builder().status(200).message(String.format("Timeline %s Successfully.", isUpdate ? "Updated" : "Saved")).timestamp(System.currentTimeMillis()).build();
    }

    //TODO check input
    @Override
    public ResponseEntity setTime(UserEntity userEntity, ScheduleHasTimeLineEntity input) {
        ScheduleHasTimeLineEntity sht = null;
        boolean isUpdate = input.getShtId() != 0;
        boolean isDelete = (input.getTimelineId() == 0 || input.getScheduleId() == 0) && isUpdate;

        if (isDelete) {
            ScheduleHasTimeLineEntity s = scheduleService.getShtById(input.getShtId());
            TimeLineEntity timeLineEntity = s.getTimeLine();
            ScheduleEntity scheduleEntity = s.getSchedule();
            if (timeLineEntity.getUserId() != userEntity.getUserId()
                    || scheduleEntity.getUserId() != userEntity.getUserId())
                throw new NotFoundException();

            scheduleService.deleteSHT(input.getShtId());
            return new ResponseObject.Builder().status(200).message("SHT Deleted Successfully.").timestamp(System.currentTimeMillis()).build();
        }

        if (isUpdate) {
            sht = scheduleService.getShtById(input.getShtId());
            if (sht == null || sht.getSchedule().getUserId() != userEntity.getUserId() || sht.getTimeLine().getUserId() != userEntity.getUserId())
                throw new NotFoundException();


        } else {
            TimeLineEntity timeLineEntity = scheduleService.getTimelineById(input.getTimelineId());
            ScheduleEntity scheduleEntity = scheduleService.getScheduleById(input.getScheduleId());
            input.setTimeLine(timeLineEntity);
            input.setSchedule(scheduleEntity);

            if (timeLineEntity == null || timeLineEntity.getUserId() != userEntity.getUserId()
                    || scheduleEntity == null || scheduleEntity.getUserId() != userEntity.getUserId())
                throw new NotFoundException();
        }

        if (sht == null) sht = input;

        scheduleService.setTime(input);

        return new ResponseObject.Builder().status(200).message(String.format("Schedule for Timeline %s Successfully %s.", sht.getTimeLine().getName(), isUpdate ? "Updated" : "Saved")).timestamp(System.currentTimeMillis()).build();
    }

    @Override
    public List<TimeLineEntity> getUserTimelines(UserEntity userEntity) {
        return scheduleService.getUserTimelines(userEntity.getUserId());
    }

    @Override
    public TimeLineEntity getTimeline(UserEntity userEntity, long timelineId) {
        TimeLineEntity timeLineEntity = scheduleService.getTimelineById(timelineId);
        if (timeLineEntity == null || timeLineEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        return timeLineEntity;
    }

    @Override
    public ResponseEntity deleteTimeline(UserEntity userEntity, long timelineId) {
        TimeLineEntity timeLineEntity = scheduleService.getTimelineById(timelineId);
        if (timeLineEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        scheduleService.deleteTimeline(timelineId);

        return new ResponseObject.Builder().status(200).message("Timeline Deleted Successfully").timestamp(System.currentTimeMillis()).build();
    }
}
