package ir.markazandroid.UniEngine.controller.userApi.interfaces;

import ir.markazandroid.UniEngine.JSONParser.JsonProfile;
import ir.markazandroid.UniEngine.persistance.entity.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Ali on 8/31/2019.
 */

@RequestMapping(value = "/userApi/schedule", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface UserApiScheduleController {

    @GetMapping("/list")
    JsonProfile getSchedules(UserEntity userEntity);

    @GetMapping
    ScheduleEntity getScheduleDetails(UserEntity userEntity, @RequestParam int scheduleId);

    @PostMapping("/create")
    ScheduleEntity createSchedule(UserEntity userEntity, @RequestParam String name);

    @PostMapping("/update")
    ResponseEntity updateSchedule(UserEntity userEntity, @RequestParam int scheduleId, @RequestParam String name);

    @PostMapping("/delete")
    ResponseEntity deleteSchedule(UserEntity userEntity, @RequestParam int scheduleId);


    @PostMapping("/assign")
    ResponseEntity assignSchedule(UserEntity userEntity, @RequestParam int scheduleId, @RequestParam long deviceId);

    @PostMapping("/unassign")
    ResponseEntity unAssignSchedule(UserEntity userEntity, @RequestParam long deviceId);

    @GetMapping("/getAssignedDevices")
    List<DeviceEntity> getAssignedDevices(UserEntity userEntity, @RequestParam int scheduleId);

    @PostMapping(path = "/timeline/saveOrUpdate", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity saveOrUpdateTimeline(UserEntity userEntity, @RequestBody TimeLineEntity timeLine);

    @PostMapping(path = "/timeline/setTime", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity setTime(UserEntity userEntity, @RequestBody ScheduleHasTimeLineEntity input);

    @GetMapping(path = "/timeline/list")
    List<TimeLineEntity> getUserTimelines(UserEntity userEntity);

    @GetMapping(path = "/timeline")
    TimeLineEntity getTimeline(UserEntity userEntity, @RequestParam long timelineId);

    @PostMapping("/timeline/delete")
    ResponseEntity deleteTimeline(UserEntity userEntity, @RequestParam long timelineId);

}
