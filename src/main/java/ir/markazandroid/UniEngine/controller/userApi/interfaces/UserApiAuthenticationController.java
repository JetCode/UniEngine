package ir.markazandroid.UniEngine.controller.userApi.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Ali on 10/12/2019.
 */
@RequestMapping(value = "/userApi/authentication", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface UserApiAuthenticationController {

    @PostMapping("/register")
    ResponseEntity register(UserEntity input);

}
