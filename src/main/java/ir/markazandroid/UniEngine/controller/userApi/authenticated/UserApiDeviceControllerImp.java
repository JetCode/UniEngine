package ir.markazandroid.UniEngine.controller.userApi.authenticated;

import ir.markazandroid.UniEngine.JSONParser.JsonProfile;
import ir.markazandroid.UniEngine.command.SocketMessageController;
import ir.markazandroid.UniEngine.controller.userApi.interfaces.UserApiDeviceController;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.object.WebSocketMessage;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceOnline;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceGroupEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import ir.markazandroid.UniEngine.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Ali on 6/15/2019.
 */
@RestController
public class UserApiDeviceControllerImp implements UserApiDeviceController {

    private final DeviceService deviceService;
    private final UserService userService;

    public UserApiDeviceControllerImp(DeviceService deviceService, UserService userService) {
        this.deviceService = deviceService;
        this.userService = userService;
    }

    @Override
    public DeviceEntity ownDevice(UserEntity userEntity, String name, String pass) {
        return deviceService.ownDevice(userEntity.getUserId(), name, pass);
    }

    @Override
    public Map<String, Object> getDevicesStatus(UserEntity userEntity) {
        int onlineDevices = userService.getUserOnlineDevices(userEntity.getUserId()).size();

        Map<String, Object> object = new HashMap<>();
        object.put("online", onlineDevices);
        object.put("total", userEntity.getParams().getDeviceCount());
        return object;
    }

    @Override
    public List<DeviceEntity> getDeviceList(UserEntity userEntity) {
        List<DeviceEntity> devices = deviceService.getUserDevices(userEntity.getUserId());
        devices.forEach(deviceEntity -> {
            DeviceOnline online = deviceService.getDeviceOnline(deviceEntity.getDeviceId());
            if (online != null)
                deviceEntity.setLastVisit(online.getTime().getTime());
        });
        return devices;
    }

    @Override
    public ResponseEntity createDeviceGroup(UserEntity userEntity, String name) {
        DeviceGroupEntity deviceGroupEntity = new DeviceGroupEntity();
        deviceGroupEntity.setUserId(userEntity.getUserId());
        deviceGroupEntity.setGroupName(name);

        deviceService.saveDeviceGroup(deviceGroupEntity);

        return new ResponseObject.Builder()
                .status(200)
                .message(String.format("گروه به اسم %s و ID %d با موفقیت ایجاد شد.",
                        deviceGroupEntity.getGroupName(), deviceGroupEntity.getDeviceGroupId()))
                .timestamp(System.currentTimeMillis())
                .build();
    }

    @Override
    public JsonProfile getUserDeviceGroups(UserEntity userEntity) {
        List<DeviceGroupEntity> list = deviceService.getUserDeviceGroups(userEntity.getUserId());
        return new JsonProfile(list).excludeFields("devices");
    }

    @Override
    public DeviceGroupEntity getGroupDetails(UserEntity userEntity, long groupId) {
        DeviceGroupEntity deviceGroupEntity = deviceService.getDeviceGroupById(groupId);
        if (deviceGroupEntity == null || deviceGroupEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        return deviceGroupEntity;
    }

    @Override
    public ResponseEntity addToGroup(UserEntity userEntity, long groupId, long deviceId) {
        DeviceGroupEntity deviceGroupEntity = deviceService.getDeviceGroupById(groupId);
        DeviceEntity deviceEntity = deviceService.getDeviceById(deviceId);

        if (deviceEntity == null || deviceEntity.getUserId() == null || deviceEntity.getUserId() != userEntity.getUserId()
                || deviceGroupEntity == null || deviceGroupEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();


        deviceService.addToGroup(groupId, deviceId);

        return new ResponseObject.Builder()
                .status(200)
                .message(String.format("دستگاه %s به گروه %s با موفقیت اضافه شد.",
                        deviceEntity.getName(), deviceGroupEntity.getGroupName()))
                .timestamp(System.currentTimeMillis())
                .build();
    }

    @Override
    public ResponseEntity removeFromGroup(UserEntity userEntity, long groupId, long deviceId) {
        DeviceGroupEntity deviceGroupEntity = deviceService.getDeviceGroupById(groupId);
        DeviceEntity deviceEntity = deviceService.getDeviceById(deviceId);

        if (deviceEntity == null || deviceEntity.getUserId() == null || deviceEntity.getUserId() != userEntity.getUserId()
                || deviceGroupEntity == null || deviceGroupEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();


        deviceService.removeFromGroup(groupId, deviceId);

        return new ResponseObject.Builder()
                .status(200)
                .message(String.format("دستگاه %s از گروه %s با موفقیت حذف شد.",
                        deviceEntity.getName(), deviceGroupEntity.getGroupName()))
                .timestamp(System.currentTimeMillis())
                .build();
    }

    @Override
    public ResponseEntity updateGroup(UserEntity userEntity, DeviceGroupEntity deviceGroup) {
        DeviceGroupEntity deviceGroupEntity = deviceService.getDeviceGroupById(deviceGroup.getDeviceGroupId());

        if (deviceGroupEntity == null || deviceGroupEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        deviceGroup.setUserId(userEntity.getUserId());

        deviceService.updateGroup(deviceGroup);

        return new ResponseObject.Builder()
                .status(200)
                .message(String.format("گروه %s با موفقیت ویرایش شد.",
                        deviceGroupEntity.getGroupName()))
                .timestamp(System.currentTimeMillis())
                .build();
    }

    @Override
    public ResponseEntity deleteGroup(UserEntity userEntity, long groupId) {
        DeviceGroupEntity deviceGroupEntity = deviceService.getDeviceGroupById(groupId);

        if (deviceGroupEntity == null || deviceGroupEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        deviceService.deleteGroup(groupId);

        return new ResponseObject.Builder()
                .status(200)
                .message(String.format("گروه %s با موفقیت حذف شد.",
                        deviceGroupEntity.getGroupName()))
                .timestamp(System.currentTimeMillis())
                .build();
    }

    @Override
    public DeviceData getDeviceData(UserEntity userEntity, long deviceId) {
        DeviceEntity deviceEntity = deviceService.getDeviceById(deviceId);
        if (deviceEntity == null || deviceEntity.getUserId() == null || deviceEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        DeviceData deviceData = deviceService.getLatestDeviceData(deviceId);
        if (deviceData == null) deviceData = new DeviceData();

        return deviceData;
    }

    @Override
    public DeviceOnline getDeviceOnlineStatus(UserEntity userEntity, long deviceId) {
        DeviceEntity deviceEntity = deviceService.getDeviceById(deviceId);
        if (deviceEntity == null || deviceEntity.getUserId() == null || deviceEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        DeviceOnline deviceOnline = deviceService.getDeviceOnline(deviceId);
        if (deviceOnline == null) deviceOnline = new DeviceOnline();

        return deviceOnline;
    }


    @Autowired
    private SocketMessageController socketMessageController;

    public ResponseEntity sendCommand(long deviceId, String command) {
        WebSocketMessage message = new WebSocketMessage();
        message.setDeviceId(deviceId);
        message.setFrom("User");
        message.setMessage(command);
        message.setTime(System.currentTimeMillis());
        message.setType(WebSocketMessage.COMMAND);
        message.setMessageId(UUID.randomUUID().toString());
        socketMessageController.sendMessage(message);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity invalidate(long deviceId) {
        deviceService.logoutDevice(deviceId);
        return ResponseEntity.ok().build();
    }
}
