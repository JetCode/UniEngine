package ir.markazandroid.UniEngine.controller.userApi.authenticated;

import ir.markazandroid.UniEngine.controller.userApi.interfaces.UserApiLayoutController;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.media.layout.*;
import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;
import ir.markazandroid.UniEngine.persistance.entity.LayoutEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.service.interfaces.CampaignService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ali on 7/24/2019.
 */
@RestController
public class UserApiLayoutControllerImp implements UserApiLayoutController {

    private final CampaignService campaignService;

    public UserApiLayoutControllerImp(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @Override
    public JSViewPort getViewPort(String orientation) {
        return JSViewPort.getDefaultViewPort(Orientation.valueOf(orientation));
    }

    @Override
    public List<JsonLayout> getLayouts(UserEntity userEntity, String orientation, String onlyUser) {
        List<LayoutEntity> layoutEntities;
        if (onlyUser != null)
            layoutEntities = campaignService.getUserLayouts(userEntity.getUserId());
        else {
            if (orientation == null) orientation = Orientation.vertical.name();
            layoutEntities = campaignService.getUserAvailableLayouts(userEntity.getUserId(), orientation);
        }

        return layoutEntities.stream().map(layoutEntity -> {
            JSViewPort viewPort = JSViewPort.getDefaultViewPort(Orientation.valueOf(layoutEntity.getOrientation()));
            JsonLayout jsonLayout = LayoutUtils.buildJsonLayout(new Layout(layoutEntity.getLayoutData()), viewPort);

            jsonLayout.setUserId(layoutEntity.getUserId());
            jsonLayout.setLayoutId(layoutEntity.getLayoutId());

            return jsonLayout;

        }).collect(Collectors.toList());
    }

    @Override
    public JsonLayout getLayout(UserEntity userEntity, long layoutId) {
        LayoutEntity layoutEntity = campaignService.getLayoutById(layoutId);

        if (layoutEntity == null || (layoutEntity.getUserId() != null && layoutEntity.getUserId() != userEntity.getUserId()))
            throw new NotFoundException();

        JSViewPort viewPort = JSViewPort.getDefaultViewPort(Orientation.valueOf(layoutEntity.getOrientation()));
        JsonLayout jsonLayout = LayoutUtils.buildJsonLayout(new Layout(layoutEntity.getLayoutData()), viewPort);

        jsonLayout.setUserId(layoutEntity.getUserId());
        jsonLayout.setLayoutId(layoutEntity.getLayoutId());

        return jsonLayout;

    }

    @Override
    public ResponseEntity newLayout(UserEntity userEntity, JsonLayout jsonLayout) {
        JSViewPort viewPort = JSViewPort.getDefaultViewPort(Orientation.valueOf(jsonLayout.getOrientation()));
        Layout layout = LayoutUtils.buildLayout(jsonLayout, viewPort);

        LayoutEntity layoutEntity = new LayoutEntity();
        layoutEntity.setUserId(userEntity.getUserId());
        layoutEntity.setOrientation(layout.getOrientation().name());
        layoutEntity.setLayoutData(layout.toLayoutData());

        campaignService.saveLayout(layoutEntity);
        return new ResponseObject.Builder().status(200).message("Layout Saved Successfully.").timestamp(System.currentTimeMillis()).build();

    }

    @Override
    public ResponseEntity deleteLayout(UserEntity userEntity, long layoutId) {
        LayoutEntity layoutEntity = campaignService.getLayoutById(layoutId);
        if (layoutEntity == null || layoutEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();
        List<String> candidates = campaignService.getUserCampaigns(userEntity.getUserId())
                .stream().filter(campaignEntity -> campaignEntity.getLayoutId() == layoutEntity.getLayoutId())
                .map(CampaignEntity::getName)
                .collect(Collectors.toList());

        if (!candidates.isEmpty()) {
            return new ResponseObject.Builder().status(HttpStatus.NOT_ACCEPTABLE.value()).message(String.format("از این چینش در کمپین(های) %s استفاده شده است. لطفا ابتدا این کمپین(ها) را ویرایش یا حذف نمایید.",
                    StringUtils.collectionToDelimitedString(candidates, " و "))).timestamp(System.currentTimeMillis()).build();
        } else {
            campaignService.deleteLayout(layoutId);
            return new ResponseObject.Builder().status(200).message("چینش حذف شد.").timestamp(System.currentTimeMillis()).build();
        }

    }
}
