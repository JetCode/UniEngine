package ir.markazandroid.UniEngine.controller.userApi;

import ir.markazandroid.UniEngine.controller.userApi.interfaces.UserApiAuthenticationController;
import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.service.interfaces.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

/**
 * Created by Ali on 10/12/2019.
 */
@RestController
public class UserApiAuthenticationControllerImp implements UserApiAuthenticationController {

    private final static Pattern farsiLettersPattern = Pattern.compile("[آ-ی\\s]*");
    private final static Pattern phonePattern = Pattern.compile("^[0][9][0-9]{9}$");
    private final static Pattern emailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private final static Pattern numberPattern = Pattern.compile("[0-9\\s]*");

    private final UserService userService;

    public UserApiAuthenticationControllerImp(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity register(UserEntity input) {

        //name
        if (input.getFirstName() == null)
            return throwError("نام نمی تواند خالی باشد.");
        input.setFirstName(input.getFirstName().trim());
        if (input.getFirstName().length() > 20)
            return throwError("نام باید حداکثر 20 حرف باشد.");
        if (!isPersianString(input.getFirstName()))
            return throwError("نام باید شامل فقط حروف فارسی باشد.");

        //lastname
        if (input.getLastName() == null)
            return throwError("نام خانوادگی نمی تواند خالی باشد.");
        input.setLastName(input.getLastName().trim());
        if (input.getLastName().length() > 30)
            return throwError("نام خانوادگی باید حداکثر 30 حرف باشد.");
        if (!isPersianString(input.getLastName()))
            return throwError("نام خانوادگی باید شامل فقط حروف فارسی باشد.");

        //phone
        if (input.getPhone() == null)
            return throwError("شماره تلفن همراه نمی تواند خالی باشد.");
        input.setPhone(input.getPhone().trim());
        if (!isPhone(input.getPhone()))
            return throwError("شماره تلفن همراه صحیح نیست.");

        //email
        if (input.getEmail() == null)
            return throwError("ایمیل نمی تواند خالی باشد.");
        input.setEmail(input.getEmail().trim().toLowerCase());
        if (input.getEmail().length() > 150)
            return throwError("ایمیل بیش از حد طولانی است!");
        if (!isEmail(input.getEmail()))
            return throwError("ایمیل صحیح نیست.");
        if (userService.getUserByEmail(input.getEmail()) != null)
            return throwError("با این آدرس ایمیل قبلا حسابی ایجاد شده است. آیا رمز عبور خود را فراموش کرده اید؟");

        if (input.getPassword() == null)
            return throwError("رمز عبور نمی تواند خالی باشد.");
        if (input.getPassword().trim().length() < 8)
            return throwError("رمز عبور باید حداقل 8 کارکتر باشد.");
        if (input.getPassword().trim().length() > 30)
            return throwError("رمز عبور باید حداکثر 30 کارکتر باشد.");


        if (UserEntity.USER_TYPE_COMPANY.equals(input.getType())) {

            //company melli
            if (input.getMeliCode() == null)
                return throwError("شناسه ملی شرکت نمی تواند خالی باشد.");
            input.setMeliCode(input.getMeliCode().trim());
            if (!checkCompanyCodeMeli(input.getMeliCode()))
                return throwError("شناسه ملی شرکت صحیح نیست.");
            if (userService.getUserByMeliCode(input.getMeliCode()) != null)
                return throwError("شرکتی قبلا با این شناسه ملی ثبت نام کرده است.");

            //company name
            if (input.getCompanyName() == null)
                return throwError("نام شرکت نمی تواند خالی باشد.");
            input.setCompanyName(input.getCompanyName().trim());
            if (!isPersianString(input.getCompanyName()))
                return throwError("نام شرکت باید شامل فقط حروف فارسی باشد.");
        } else if (UserEntity.USER_TYPE_NORMAL.equals(input.getType())) {
            //user melli
            if (input.getMeliCode() == null)
                return throwError("کد ملی نمی تواند خالی باشد.");
            input.setMeliCode(input.getMeliCode().trim());
            if (!isValidNationalCode(input.getMeliCode()))
                return throwError("کد ملی صحیح نیست.");
            if (userService.getUserByMeliCode(input.getMeliCode()) != null)
                return throwError("کاربری قبلا با این کد ملی ثبت نام کرده است.");

        } else
            return throwError("نوع حساب صحیح نیست.");

        userService.register(input);

        return new ResponseObject.Builder().status(200).message("ثبت نام با موفقیت انجام شد. ایمیلی حاوی لینک فعالسازی اکانت به ایمیل شما ارسال شده است.").timestamp(System.currentTimeMillis()).build();

    }


    private ResponseEntity throwError(String s) {
        return new ResponseObject.Builder()
                .status(406)
                .timestamp(System.currentTimeMillis())
                .message(s)
                .build();
    }


    private static boolean isPersianString(String s) {
        return
                s != null
                        && !s.isEmpty() &&
                        farsiLettersPattern.matcher(s).matches();
    }

    public static void main(String[] args) {
        System.out.println(isPersianString("علی"));
    }

    public static boolean isEmail(String s) {
        return s != null && !s.isEmpty() && emailPattern.matcher(s).matches();
    }

    private static boolean isPhone(String s) {
        return s != null && !s.isEmpty() && phonePattern.matcher(s).matches();
    }

    public static boolean isValidNationalCode(String nationalCode) {
        if (nationalCode == null || nationalCode.isEmpty())
            return false;

        else if (!numberPattern.matcher(nationalCode).matches())
            return false;

        else if (nationalCode.length() != 10)
            return false;

        String[] arrayAllSameDigits = new String[]{"0000000000", "1111111111", "2222222222",
                "3333333333", "4444444444", "5555555555", "6666666666",
                "7777777777", "8888888888", "9999999999"};

        for (String item : arrayAllSameDigits) {
            if (item.equals(nationalCode))
                return false;
        }

        char[] arrayChars = nationalCode.toCharArray();

        int one = parseInt(String.valueOf(arrayChars[0])) * 10;
        int two = parseInt(String.valueOf(arrayChars[1])) * 9;
        int three = parseInt(String.valueOf(arrayChars[2])) * 8;
        int four = parseInt(String.valueOf(arrayChars[3])) * 7;
        int five = parseInt(String.valueOf(arrayChars[4])) * 6;
        int six = parseInt(String.valueOf(arrayChars[5])) * 5;
        int seven = parseInt(String.valueOf(arrayChars[6])) * 4;
        int eight = parseInt(String.valueOf(arrayChars[7])) * 3;
        int nine = parseInt(String.valueOf(arrayChars[8])) * 2;

        int ten = parseInt(String.valueOf(arrayChars[9]));

        int b = (((((((one + two) + three) + four) + five) + six) + seven) + eight) + nine;
        int c = b % 11;

        return (((c < 2) && (ten == c)) || ((c >= 2) && ((11 - c) == ten)));

    }


    private static boolean checkCompanyCodeMeli(String code) {

        int L = code.length();

        if (L < 11 || Long.parseLong(code) == 0) return false;

        if (parseInt(code.substring(3, 9), 10) == 0) return false;
        int c = parseInt(code.substring(10, 11), 10);
        int d = parseInt(code.substring(9, 10), 10) + 2;
        int[] z = new int[]{29, 27, 23, 19, 17};
        int s = 0;
        for (int i = 0; i < 10; i++)
            s += (d + parseInt(code.substring(i, i + 1), 10)) * z[i % 5];
        s = s % 11;
        if (s == 10) s = 0;
        return (c == s);

    }


}
