package ir.markazandroid.UniEngine.controller.userApi.authenticated;

import ir.markazandroid.UniEngine.controller.userApi.UserApiAuthenticationControllerImp;
import ir.markazandroid.UniEngine.controller.userApi.interfaces.UserApiUserController;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.exception.VerificationCodeAlreadySentException;
import ir.markazandroid.UniEngine.exception.VerificationCodeExpiredException;
import ir.markazandroid.UniEngine.exception.VerificationCodeIncorrectException;
import ir.markazandroid.UniEngine.object.ErrorObject;
import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import ir.markazandroid.UniEngine.service.interfaces.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Locale;

/**
 * Created by Ali on 6/25/2018.
 */
@RestController
public class UserApiUserControllerImp implements UserApiUserController {

    private final UserService userService;
    private final DeviceService deviceService;

    public UserApiUserControllerImp(UserService userService, DeviceService deviceService) {
        this.userService = userService;
        this.deviceService = deviceService;
    }

    @Override
    public UserEntity getMe(UserEntity userEntity){
        return userEntity;
    }

    @Override
    public ResponseEntity resendVerificationEmail(UserEntity userEntity, String email) {
        if (userEntity.getStatus() != UserEntity.STATUS_NOT_VERIFIED)
            throw new NotFoundException();

        //TODO make these email verification codes singular with UserApiAuthenticationControllerImp
        if (email != null) {
            //email
            email = email.trim().toLowerCase();
            if (email.length() > 150)
                return throwError("ایمیل بیش از حد طولانی است!");
            if (!UserApiAuthenticationControllerImp.isEmail(email))
                return throwError("ایمیل صحیح نیست.");
            if (userService.getUserByEmail(email) != null)
                return throwError("با این آدرس ایمیل قبلا حسابی ایجاد شده است. آیا رمز عبور خود را فراموش کرده اید؟");

            userService.changeUserEmail(userEntity.getUserId(), email);
        }

        userService.sendEmailVerificationCode(userEntity.getUserId(), new Locale("fa", ""));
        return new ResponseObject.Builder().status(200).message("ایمیل فعالسازی ارسال شد.").timestamp(System.currentTimeMillis()).build();
    }

    @Override
    public ResponseEntity verify(UserEntity userEntity, String code) {
        userService.verifyUser(code, userEntity.getUserId());
        return new ResponseObject.Builder().status(200).message("حساب کاربری با موفقیت فعال شد.").timestamp(System.currentTimeMillis()).build();
    }

    @Override
    public List<DeviceEntity> getDevicesMadeForUser(UserEntity userEntity) {
        return deviceService.getDevicesMadeForUser(userEntity.getUserId());
    }

    private ResponseEntity throwError(String s) {
        return new ResponseObject.Builder()
                .status(406)
                .timestamp(System.currentTimeMillis())
                .message(s)
                .build();
    }

    @ExceptionHandler(VerificationCodeAlreadySentException.class)
    @ResponseBody
    public ResponseEntity<ErrorObject> handleVerificationSmsAlreadySentException() {
        ErrorObject errorObject = new ErrorObject();
        errorObject.setStatus(HttpStatus.BAD_REQUEST.value());
        errorObject.setMessage("کد تایید به تازگی فرستاده شده است. لطفا اندکی صبر کنید.");
        errorObject.setTimestamp(System.currentTimeMillis());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON_UTF8).body(errorObject);
    }

    @ExceptionHandler(VerificationCodeIncorrectException.class)
    @ResponseBody
    public ResponseEntity<ErrorObject> handleVerificationCodeIncorrectException() {
        ErrorObject errorObject = new ErrorObject();
        errorObject.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
        errorObject.setMessage("کد تایید نادرست است.");
        errorObject.setTimestamp(System.currentTimeMillis());
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .contentType(MediaType.APPLICATION_JSON_UTF8).body(errorObject);
    }

    @ExceptionHandler(VerificationCodeExpiredException.class)
    @ResponseBody
    public ResponseEntity<ErrorObject> handleVerificationCodeExpiredException() {
        ErrorObject errorObject = new ErrorObject();
        errorObject.setStatus(HttpStatus.REQUEST_TIMEOUT.value());
        errorObject.setMessage("کد تایید منقضی شده است.");
        errorObject.setTimestamp(System.currentTimeMillis());
        return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT)
                .contentType(MediaType.APPLICATION_JSON_UTF8).body(errorObject);
    }

}
