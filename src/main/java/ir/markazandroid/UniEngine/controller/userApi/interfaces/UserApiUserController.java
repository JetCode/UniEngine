package ir.markazandroid.UniEngine.controller.userApi.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Ali on 10/12/2019.
 */
@RequestMapping(value = "/userApi/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface UserApiUserController {

    @RequestMapping("/getMe")
    UserEntity getMe(UserEntity userEntity);

    @PostMapping("/resendEmail")
    ResponseEntity resendVerificationEmail(UserEntity userEntity, @RequestParam(required = false) String email);

    @PostMapping("/verify")
    ResponseEntity verify(UserEntity userEntity, String code);

    //TODO give Mamad
    @GetMapping("/getDevicesMadeFor")
    List<DeviceEntity> getDevicesMadeForUser(UserEntity userEntity);

}
