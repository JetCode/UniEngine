package ir.markazandroid.UniEngine.controller.userApi.interfaces;

import ir.markazandroid.UniEngine.media.layout.JSViewPort;
import ir.markazandroid.UniEngine.media.layout.JsonLayout;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Ali on 7/20/2019.
 */
@RequestMapping(value = "/userApi/layout", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface UserApiLayoutController {

    @GetMapping("/getViewPort")
    JSViewPort getViewPort(@RequestParam(required = false, defaultValue = "vertical") String orientation);

    @GetMapping("/list")
    List<JsonLayout> getLayouts(UserEntity userEntity, @RequestParam(required = false) String orientation, @RequestParam(required = false) String onlyUser);

    @GetMapping
    JsonLayout getLayout(UserEntity userEntity, @RequestParam long layoutId);

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity newLayout(UserEntity userEntity, @RequestBody JsonLayout jsonLayout);

    @PostMapping("/delete")
    ResponseEntity deleteLayout(UserEntity userEntity, @RequestParam long layoutId);

}
