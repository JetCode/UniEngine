package ir.markazandroid.UniEngine.controller.userApi.interfaces;

import ir.markazandroid.UniEngine.JSONParser.JsonProfile;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceOnline;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceGroupEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Ali on 4/13/2019.
 */
@RequestMapping(value = "/userApi/device", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface UserApiDeviceController {

    @PostMapping("/own")
    DeviceEntity ownDevice(UserEntity userEntity, @RequestParam String name, @RequestParam String pass);

    @GetMapping("/status")
    Map<String, Object> getDevicesStatus(UserEntity userEntity);

    @GetMapping("/list")
    List<DeviceEntity> getDeviceList(UserEntity userEntity);

    @PostMapping("/group/create")
    ResponseEntity createDeviceGroup(UserEntity userEntity, @RequestParam String name);

    @GetMapping("/group/list")
    JsonProfile getUserDeviceGroups(UserEntity userEntity);

    @GetMapping("/group")
    DeviceGroupEntity getGroupDetails(UserEntity userEntity, @RequestParam long groupId);

    @PostMapping("/group/add")
    ResponseEntity addToGroup(UserEntity userEntity, @RequestParam long groupId, @RequestParam long deviceId);

    @PostMapping("/group/remove")
    ResponseEntity removeFromGroup(UserEntity userEntity, @RequestParam long groupId, @RequestParam long deviceId);

    @PostMapping(value = "/group/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity updateGroup(UserEntity userEntity, @RequestBody DeviceGroupEntity deviceGroup);

    @PostMapping("/group/delete")
    ResponseEntity deleteGroup(UserEntity userEntity, @RequestParam long groupId);

    @GetMapping("/data")
    DeviceData getDeviceData(UserEntity userEntity, @RequestParam long deviceId);

    @GetMapping("/online")
    DeviceOnline getDeviceOnlineStatus(UserEntity userEntity, @RequestParam long deviceId);

    @RequestMapping("/sendCommand")
    ResponseEntity sendCommand(@RequestParam long deviceId, @RequestParam String command);

    @RequestMapping("/invalidate")
    ResponseEntity invalidate(@RequestParam long deviceId);

}
