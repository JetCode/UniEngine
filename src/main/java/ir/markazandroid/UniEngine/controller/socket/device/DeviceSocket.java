package ir.markazandroid.UniEngine.controller.socket.device;

import ir.markazandroid.UniEngine.command.SocketMessageController;
import ir.markazandroid.UniEngine.object.WebSocketConfiguration;
import ir.markazandroid.UniEngine.object.WebSocketMessage;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.UUID;

/**
 * Created by Ali on 1/29/2019.
 */
@Controller
public class DeviceSocket {


    private final SimpMessageSendingOperations messagingTemplate;

    private final SocketMessageController socketMessageController;


    @Autowired
    public DeviceSocket(SocketMessageController socketMessageController, SimpMessageSendingOperations messagingTemplate) {
        this.socketMessageController = socketMessageController;
        this.messagingTemplate = messagingTemplate;
    }

    @MessageMapping("/sendToUser")
    //@SendToUser("/queue/reply")
    public void sendToUser(@Payload WebSocketMessage msg, Principal user, @Header("simpSessionId") String sessionId
            , SimpMessageHeaderAccessor headerAccessor/*, org.springframework.messaging.Message ms*/) {
        DeviceEntity deviceEntity = (DeviceEntity) headerAccessor.getSessionAttributes().get("deviceEntity");
        System.out.println(msg.toString());
        System.out.println(deviceEntity.getName());
        msg.setFrom(deviceEntity.getName());
        msg.setDeviceId(deviceEntity.getDeviceId());
        //template.convertAndSendToUser("Ali","/queue/reply",out);
        socketMessageController.messageReceived(msg);
        messagingTemplate.convertAndSendToUser("user_" + deviceEntity.getUserId(), "/queue/user", msg);
        socketMessageController.sendWaitingMessagesToDevice(deviceEntity.getDeviceId());
        if (WebSocketMessage.CONNECTED.equals(msg.getType())) {
            messagingTemplate.convertAndSendToUser("device_" + deviceEntity.getDeviceId(), "/queue/device", onSubscribe(headerAccessor));
        }
        //simpMessagingTemplate.convertAndSend("/secured/user/queue/Ali",out);
        System.out.println("sessionId: " + sessionId);
        //return out;
    }

    @MessageMapping("/ping")
    @SendToUser("/queue/device")
    public void ping(@Payload WebSocketMessage msg, SimpMessageHeaderAccessor headerAccessor) {
        DeviceEntity deviceEntity = (DeviceEntity) headerAccessor.getSessionAttributes().get("deviceEntity");

        WebSocketMessage message = new WebSocketMessage();
        message.setDeviceId(deviceEntity.getDeviceId());
        message.setFrom("Server");
        message.setMessage("non");
        message.setTime(System.currentTimeMillis());
        message.setType(WebSocketMessage.PONG);
        message.setMessageId(msg.getMessageId());

        messagingTemplate.convertAndSendToUser("device_" + deviceEntity.getDeviceId(), "/queue/device", onSubscribe(headerAccessor));

    }


    @SubscribeMapping("/user/queue/device")
    public WebSocketConfiguration onSubscribe(SimpMessageHeaderAccessor headerAccessor) {
        DeviceEntity deviceEntity = (DeviceEntity) headerAccessor.getSessionAttributes().get("deviceEntity");

        WebSocketConfiguration configuration = new WebSocketConfiguration();
        configuration.setDeviceId(deviceEntity.getDeviceId());
        configuration.setFrom("Server");
        configuration.setMessage("non");
        configuration.setTime(System.currentTimeMillis());
        configuration.setType(WebSocketMessage.COMMAND);
        configuration.setMessageId(UUID.randomUUID().toString());

        configuration.setNoMessageTimeout(120_000);
        configuration.setPingTime(3_000);
        configuration.setNoPongTimeout(12_000);

        System.out.println("config sent");
        return configuration;
    }

}
