package ir.markazandroid.UniEngine.controller.socket.user;

import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.object.ErrorObject;
import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.IntervalConfig;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

/**
 * Created by Ali on 1/29/2019.
 */
@Controller
public class UserSocket {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    private final TaskScheduler poolScheduler;
    private Map<String, ScheduledFuture> intervals;
    private final DeviceService deviceService;

    public UserSocket(TaskScheduler poolScheduler, DeviceService deviceService) {
        this.poolScheduler = poolScheduler;
        this.deviceService = deviceService;
        intervals = new HashMap<>();
    }


    @MessageMapping("/enableInterval")
    @SendToUser("/queue/user")
    public ResponseObject enableInterval(@Payload IntervalConfig intervalConfig, SimpMessageHeaderAccessor headerAccessor) {
        UserEntity userEntity = (UserEntity) headerAccessor.getSessionAttributes().get("userEntity");
        if (userEntity == null) throw new RuntimeException("Details null");

        DeviceEntity deviceEntity = deviceService.getDeviceById(intervalConfig.getDeviceId());
        if (deviceEntity == null || deviceEntity.getUserId() != userEntity.getUserId())
            throw new NotFoundException();

        ScheduledFuture lastScheduledFuture = intervals.get(headerAccessor.getSessionId());
        if (lastScheduledFuture != null) lastScheduledFuture.cancel(false);

        ScheduledFuture scheduledFuture = poolScheduler.scheduleAtFixedRate(() -> {
            DeviceData deviceData = deviceService.getLatestDeviceData(intervalConfig.getDeviceId());
            if (deviceData == null) deviceData = new DeviceData();
            sendToSession(headerAccessor.getSessionId(), deviceData);
        }, intervalConfig.getInterval() * 1000);


        intervals.put(headerAccessor.getSessionId(), scheduledFuture);

        return new ResponseObject.Builder().message(String.format("%d second(s) Interval For device %s Successfully set", intervalConfig.getInterval(), deviceEntity.getName()))
                .status(200).timestamp(System.currentTimeMillis()).buildOriginal();
    }


    @MessageExceptionHandler
    public void onException(Exception e, SimpMessageHeaderAccessor headerAccessor) {
        ErrorObject errorObject = new ErrorObject();
        int errorCode = 500;
        if (e.getClass().isAnnotationPresent(ResponseStatus.class)) {
            ResponseStatus status = e.getClass().getAnnotation(ResponseStatus.class);
            errorCode = status.value().value();
        }
        errorObject.setStatus(errorCode);
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(System.currentTimeMillis());
        sendToSession(headerAccessor.getSessionId(), errorObject);
    }


    @GetMapping("websocket")
    public String getSockClinet() {
        return "websocketTestServer";
    }


    @EventListener(SessionDisconnectEvent.class)
    public void userDisconnected(SessionDisconnectEvent event) {
        SimpMessageHeaderAccessor accessor = SimpMessageHeaderAccessor.wrap(event.getMessage());
        ScheduledFuture lastScheduledFuture = intervals.remove(accessor.getSessionId());
        if (lastScheduledFuture != null) lastScheduledFuture.cancel(false);
        System.out.println("Canceled Session: " + accessor.getSessionId());
    }

    public void sendToSession(String sessionId, Object payload) {
        SimpMessageHeaderAccessor ha = SimpMessageHeaderAccessor
                .create(SimpMessageType.MESSAGE);
        ha.setSessionId(sessionId);
        ha.setLeaveMutable(true);

        messagingTemplate.convertAndSendToUser(sessionId,
                "/queue/user", payload,
                ha.getMessageHeaders());
    }



    /*@SubscribeMapping("/user/queue/user")
    public WebSocketConfiguration onSubscribe(SimpMessageHeaderAccessor headerAccessor){

        WebSocketConfiguration configuration = new WebSocketConfiguration();
        configuration.setFrom("Server");
        configuration.setMessage("non");
        configuration.setTime(System.currentTimeMillis());
        configuration.setType(WebSocketMessage.COMMAND);
        configuration.setMessageId(UUID.randomUUID().toString());

        configuration.setNoMessageTimeout(30_000);
        configuration.setPingTime(5_000);
        configuration.setNoPongTimeout(10_000);

        System.out.println("config sent");
        return configuration;
    }*/
}
