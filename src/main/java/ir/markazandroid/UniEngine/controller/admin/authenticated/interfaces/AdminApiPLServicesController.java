package ir.markazandroid.UniEngine.controller.admin.authenticated.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.DeviceModelEntity;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Ali on 5/1/2019.
 */
@RequestMapping(value = "/admin/PLServices",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface AdminApiPLServicesController {

    @PostMapping("/register")
    JSONObject registerDevice(@RequestParam String name, @RequestParam(required = false, defaultValue = "na") String deviceId,
                              @RequestParam(required = false, defaultValue = "na") String bluetoothMac);

    @GetMapping("/deviceModel/list")
    List<DeviceModelEntity> getDeviceModels();

    @GetMapping("/device/nameAvailable")
    JSONObject isNameAvailable(@RequestParam String name);

}
