package ir.markazandroid.UniEngine.controller.deviceApi;

import ir.markazandroid.UniEngine.conf.session.SessionManager;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceOnline;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;

/**
 * Created by Ali on 10/9/2018.
 */
@ControllerAdvice(basePackages = "ir.markazandroid.UniEngine.controller.deviceApi")
public class DeviceApiControllerAdvice {

    private final SessionManager sessionManager;
    private final DeviceService deviceService;

    public DeviceApiControllerAdvice(SessionManager sessionManager, DeviceService deviceService) {
        this.sessionManager = sessionManager;
        this.deviceService = deviceService;
    }

    @ModelAttribute(binding = false)
    public DeviceEntity deviceEntity(HttpSession session) {
        DeviceEntity deviceEntity = (DeviceEntity) sessionManager.getPrincipal(session);
        deviceEntity.setLastVisit(System.currentTimeMillis());
        deviceService.saveDeviceOnline(new DeviceOnline(deviceEntity.getDeviceId(), deviceEntity.getLastVisit()));

        return deviceEntity;

    }


}
