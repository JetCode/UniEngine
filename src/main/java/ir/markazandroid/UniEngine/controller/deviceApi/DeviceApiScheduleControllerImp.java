package ir.markazandroid.UniEngine.controller.deviceApi;

import ir.markazandroid.UniEngine.controller.deviceApi.interfaces.DeviceApiScheduleController;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.service.interfaces.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Ali on 9/24/2019.
 */
@RestController
public class DeviceApiScheduleControllerImp implements DeviceApiScheduleController {


    private final ScheduleService scheduleService;

    @Autowired
    public DeviceApiScheduleControllerImp(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @Override
    public ResponseEntity getSchedule(DeviceEntity deviceEntity, @RequestParam long update) {
        if (deviceEntity.getLastUpdateVersion() > update) {

            if (deviceEntity.getAdvertiserScheduleId() == null)
                return ResponseEntity.noContent()
                        .header("lastUpdate", deviceEntity.getLastUpdateVersion() + "")
                        .build();

            ByteBuffer b = ByteBuffer.allocateDirect(20);
            b.order(ByteOrder.nativeOrder());

            return ResponseEntity.ok()
                        .header("lastUpdate", deviceEntity.getLastUpdateVersion() + "")
                    .body(scheduleService.getDetailedSchedule(deviceEntity.getAdvertiserScheduleId()));

        } else return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }
}
