package ir.markazandroid.UniEngine.controller.deviceApi.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ali on 6/10/2019.
 */
@RequestMapping(value = "/deviceApi/schedule", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface DeviceApiScheduleController {

    @GetMapping("/get")
    ResponseEntity getSchedule(DeviceEntity deviceEntity, @RequestParam long update);
}

