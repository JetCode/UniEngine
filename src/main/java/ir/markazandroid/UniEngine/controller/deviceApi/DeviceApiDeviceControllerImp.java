package ir.markazandroid.UniEngine.controller.deviceApi;

import com.datastax.driver.core.LocalDate;
import ir.markazandroid.UniEngine.constants.DeviceDataParams;
import ir.markazandroid.UniEngine.controller.deviceApi.interfaces.DeviceApiDeviceController;
import ir.markazandroid.UniEngine.object.ResponseObject;
import ir.markazandroid.UniEngine.object.Status;
import ir.markazandroid.UniEngine.object.Version;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by Ali on 6/10/2019.
 */
@RestController
public class DeviceApiDeviceControllerImp implements DeviceApiDeviceController {

    private final DeviceService deviceService;
    private final DeviceDataParams dataParams;

    public DeviceApiDeviceControllerImp(DeviceService deviceService, DeviceDataParams dataParams) {
        this.deviceService = deviceService;
        this.dataParams = dataParams;

    }

    boolean sent =false;

    @Override
    public DeviceEntity getMe(DeviceEntity deviceEntity) {
        return deviceEntity;
    }

    @Override
    public ResponseEntity onData(DeviceEntity deviceEntity, JSONObject jsonObject) {

        JSONArray dataArray = jsonObject.getJSONArray("datas");
        dataArray.forEach(o -> {
            JSONObject jo = (JSONObject) o;
            final long time = jo.getLong("t");
            jo.remove("t");
            if (time > System.currentTimeMillis() + 59_000 || time < System.currentTimeMillis() - (3600 * 24 * 30L))
                return;

            DeviceData deviceData = new DeviceData();
            deviceData.setDeviceId(deviceEntity.getDeviceId());
            deviceData.setTime(time);
            deviceData.setDate(LocalDate.fromMillisSinceEpoch(time));
            Map<String, Object> values = jo.toMap();
            values.forEach((s, o1) -> dataParams.setParamIntoObject(deviceData, s, o1));
            deviceService.saveDeviceData(deviceData);
        });


      /*  if (!sent && deviceEntity.getDeviceId()==513){
            Status status = new Status();
            status.setLastTime(System.currentTimeMillis());
            Version version = new Version();
            version.setVersion(1505);
            version.setName("1.5.5 DS_NEW");
            version.setModel("DS_NEW");
            version.setAllowDowngrade(false);
            version.setUrl("http://data.harajgram.ir/advertiser/devices/Police_V1.5.5_DS_NEW.apk");
            status.setVersion(version);

            return ResponseEntity.ok(status);
        }*/

        return new ResponseObject.Builder().status(200).timestamp(System.currentTimeMillis()).build();

    }
}
