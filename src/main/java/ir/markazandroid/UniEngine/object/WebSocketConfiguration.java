package ir.markazandroid.UniEngine.object;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

/**
 * Created by Ali on 2/17/2019.
 */
@JSON
public class WebSocketConfiguration extends WebSocketMessage {


    private int pingTime;
    private int noPongTimeout;
    private int noMessageTimeout;


    public WebSocketConfiguration() {
        super.setType(CONFIGURATION);
    }


    @Override
    public void setType(String type) {
        // super.setType(type);
    }

    @JSON
    public int getPingTime() {
        return pingTime;
    }

    public void setPingTime(int pingTime) {
        this.pingTime = pingTime;
    }

    @JSON
    public int getNoPongTimeout() {
        return noPongTimeout;
    }

    public void setNoPongTimeout(int noPongTimeout) {
        this.noPongTimeout = noPongTimeout;
    }

    @JSON
    public int getNoMessageTimeout() {
        return noMessageTimeout;
    }

    public void setNoMessageTimeout(int noMessageTimeout) {
        this.noMessageTimeout = noMessageTimeout;
    }
}
