package ir.markazandroid.UniEngine.object;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;

@JSON
public class Status implements Serializable {

    private long lastTime;
    private Version version;
    private String cmd;

    @JSON
    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }

    @JSON(classType = JSON.CLASS_TYPE_OBJECT,clazz = Version.class)
    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @JSON
    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }
}