package ir.markazandroid.UniEngine;

import ir.markazandroid.UniEngine.JSONParser.JsonMessageConverter;
import ir.markazandroid.UniEngine.JSONParser.Parser;
import ir.markazandroid.UniEngine.JSONParser.ParserInitializr;
import ir.markazandroid.UniEngine.notification.NotificationSender;
import ir.markazandroid.UniEngine.notification.NotificationSenderImp;
import ir.markazandroid.UniEngine.persistance.entity.PrivilegeEntity;
import ir.markazandroid.UniEngine.persistance.entity.RoleEntity;
import ir.markazandroid.UniEngine.sms.KavenegarApi;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import javax.sql.DataSource;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@EnableCaching
@EnableAsync
@SpringBootApplication
public class UniEngineApplication extends SpringBootServletInitializer {

    public static final RoleEntity ROLE_USER = new RoleEntity("USER", 1, new PrivilegeEntity(PrivilegeEntity.ABILITY_TO_ADD_DEVICE));
    public static final RoleEntity ROLE_USER_BASIC = new RoleEntity("USER_BASIC", 5, RoleEntity.mergePrivileges(ROLE_USER, new PrivilegeEntity(PrivilegeEntity.ACCESS_USER_CMS)));
    public static final RoleEntity ROLE_DEVICE = new RoleEntity("DEVICE", 4, new PrivilegeEntity(PrivilegeEntity.ACCESS_DEVICE_API));
    public static final RoleEntity ROLE_ADMIN = new RoleEntity("ADMIN", 2, new PrivilegeEntity(PrivilegeEntity.ACCESS_ADMIN_DASHBOARD));
    public static final RoleEntity ROLE_PL_AGENT = new RoleEntity("PL_AGENT", 3, new PrivilegeEntity(PrivilegeEntity.ACCESS_PL_SERVICES)
            ,new PrivilegeEntity(PrivilegeEntity.ACCESS_ADMIN_DASHBOARD));

    private static final Logger logger = LoggerFactory.getLogger(UniEngineApplication.class);


    //Every thing starts from here
	public static void main(String[] args) {
		SpringApplication.run(UniEngineApplication.class, args)/*.registerShutdownHook()*/;
	}


    @Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	//my beloved json parser ♥♥♥
	@Bean
    public JsonMessageConverter parser() {
        JsonMessageConverter parser = new JsonMessageConverter();
		ParserInitializr initializr = new ParserInitializr(parser);
		initializr.findAnnotatedClasses();
        return parser;
	}

	//for tomcat deploy
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(UniEngineApplication.class);
    }

    //for remmember me
	@Bean
	public PersistentTokenRepository jdbcTokenRepository(DataSource dataSource) {
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		//tokenRepository.setCreateTableOnStartup(true);
		return tokenRepository;
	}


	//general task executor
	@Bean
	public TaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(10);
		executor.setThreadNamePrefix("default_task_executor_thread");
		executor.initialize();
		return executor;
	}

	//general scheduler
    @Primary
    @Bean
    public TaskScheduler poolScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
        scheduler.setPoolSize(10);
        scheduler.initialize();
        return scheduler;
    }

    //Master OKHTTP ♥
	@Bean
	public OkHttpClient httpClient(){
		OkHttpClient okHttpClient = new OkHttpClient.Builder()
				.connectTimeout(60, TimeUnit.SECONDS)
				.readTimeout(60, TimeUnit.SECONDS)
				.writeTimeout(60, TimeUnit.SECONDS)
				.build();
        logger.info("OkHttp Client Created");
		return okHttpClient;
	}

	//Old notification sender (rewind is suggested)
	@Bean
	public NotificationSender notificationSender(OkHttpClient httpClient, Parser parser) throws NoSuchMethodException {
		return new NotificationSenderImp(httpClient,
				"AAAAPcDiPgc:APA91bFCuZcvUtQSuC21zEaEtroj0GjJS6ykeV4mGgfMXegGPag26h6HdyrlG4LyWo4mEGFh4spi8qnn7d89uVRaZP6EfJ4hqr7U00r0qGU8aMxwfU0AYX2UsSqPvS9BJzs8gN4mVSdG", parser);
	}

	//No use
	@Bean
	public KavenegarApi kavenegarApi(OkHttpClient client){
		return new KavenegarApi(client,"68397A3470724E70384472775A782B4B4B684E78566B3434463835444F656C6B");
	}

	//Security reasons (kinda)
	@Bean
	public SecureRandom getSecureRandom() throws NoSuchAlgorithmException {
		return SecureRandom.getInstanceStrong();
	}


	//for thymeleaf
	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver slr = new CookieLocaleResolver();
		slr.setDefaultLocale(new Locale("fa", ""));
		return slr;
	}

	//for thymeleaf
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}



    /*@Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher(){
            @Override
            public void sessionCreated(HttpSessionEvent event) {
                super.sessionCreated(event);
                            }

            @Override
            public void sessionDestroyed(HttpSessionEvent event) {
                super.sessionDestroyed(event); }
        };
    }*/

    //general event handler (debug)
    @EventListener
    public void onEvent(Object o){
        //   logger.info("Event: " + o.toString());
    }

    /*@Bean
    public HttpSessionListener httpSessionListener(){
	    return new HttpSessionListener() {
            @Override
            public void sessionCreated(HttpSessionEvent event) {
                SecurityContext imp= (SecurityContext) event.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
                System.out.println(event.getSession().getAttribute("created: "+imp.getAuthentication().getPrincipal().toString()));
            }

            @Override
            public void sessionDestroyed(HttpSessionEvent event) {
                SecurityContext imp= (SecurityContext) event.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
                System.out.println(event.getSession().getAttribute("destroyed : "+imp.getAuthentication().getPrincipal().toString()));

            }
        };
    }*/

}
