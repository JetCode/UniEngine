package ir.markazandroid.UniEngine.media.views;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.media.annotations.View;

/**
 * Created by Ali on 4/22/2019.
 */
@JSON
@View("ir.markazandroid.widget.CalendarView")
public class CalendarView extends BasicView {

    public static final String TYPE_CALENDAR = "persianCalendar";

    private String cityName="TEHRAN";
    private boolean sayAthan=false;

    public CalendarView() {
        setType(TYPE_CALENDAR);
    }


    @Override
    public Object getExtrasObject() {
        return this;
    }


    @JSON
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @JSON
    public boolean setSayAthan() {
        return sayAthan;
    }

    public void getSayAthan(boolean sayAthan) {
        this.sayAthan = sayAthan;
    }
}
