package ir.markazandroid.UniEngine.media.views;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.media.annotations.View;
import ir.markazandroid.UniEngine.media.inits.ViewInitializr;
import ir.markazandroid.UniEngine.object.EFile;
import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;
import ir.markazandroid.UniEngine.persistance.entity.PlayListEntity;
import ir.markazandroid.UniEngine.persistance.file.entity.MetaDataEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.FileMetaDataDAO;
import ir.markazandroid.UniEngine.service.interfaces.CampaignService;
import ir.markazandroid.UniEngine.service.interfaces.StorageService;
import ir.markazandroid.UniEngine.util.Utils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Predicate;

@JSON
@View("ir.markazandroid.masteradvertiser.views.PlayListView")
public class StaticImageView extends BasicView {

    public static final String TYPE_STATIC_IMAGE = "static_image";

    private EFile imageFile;

    public StaticImageView() {
        setType(TYPE_STATIC_IMAGE);
    }

    @Override
    public Object getExtrasObject() {
        PlayListEntity.Image image = new PlayListEntity.Image();
        image.setScaleType("fitXY");
        image.seteFile(imageFile);
        ArrayList<PlayListEntity.DataEntity> datas = new ArrayList<>();
        datas.add(image);

        PlayListEntity.Data data = new PlayListEntity.Data();
        data.setEntities(datas);

        PlayListView playListView = new PlayListView();
        playListView.setData(data);

        return playListView.getExtrasObject();
    }

    @JSON
    public EFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(EFile imageFile) {
        this.imageFile = imageFile;
    }


    @Component(TYPE_STATIC_IMAGE + "_initializr")
    @Scope(BeanDefinition.SCOPE_SINGLETON)
    public static class StaticImageViewInitializr implements ViewInitializr, ApplicationListener<StorageService.FileDeletedEvent> {


        private final CampaignService campaignService;
        private final StorageService storageService;
        private final FileMetaDataDAO metaDataDAO;

        public StaticImageViewInitializr(CampaignService campaignService, StorageService storageService, FileMetaDataDAO metaDataDAO) {
            this.campaignService = campaignService;
            this.storageService = storageService;
            this.metaDataDAO = metaDataDAO;
        }


        @Override
        public int doInit(BasicView basicView, CampaignEntity campaignEntity) {
            StaticImageView staticImageView = (StaticImageView) basicView;
            storageService.readFile(Utils.decodeString(staticImageView.imageFile.geteFileId()));

            metaDataDAO.saveMetaData(new MetaDataEntity(
                    Utils.decodeString(staticImageView.imageFile.geteFileId()),
                    StaticImageView.class.getName() + "@" + campaignEntity.getCampaignId()));

            return 0;
        }


        @Async
        public void onApplicationEvent(StorageService.FileDeletedEvent event) {
            String fileId = storageService.generateFileId(event.getFile());
            metaDataDAO.getFileMetaDatas(fileId)
                    .forEach(metaDataEntity -> {
                        if (metaDataEntity.getReference().startsWith(StaticImageView.class.getName())) {
                            String[] ids = metaDataEntity.getReference().split("@");
                            long campaignId = Long.parseLong(ids[1]);
                            CampaignEntity campaignEntity = campaignService.getCampaignById(campaignId);
                            if (campaignEntity != null) {

                                if (campaignEntity.getData().getViews().removeIf(basicView ->
                                        TYPE_STATIC_IMAGE.equals(basicView.getType())
                                                && Utils.encodeString(fileId).equals(((StaticImageView) basicView).getImageFile().geteFileId()))) {
                                    campaignService.saveOrUpdateCampaign(campaignEntity);
                                }
                            }
                            metaDataDAO.deleteMetaData(metaDataEntity);
                        }
                    });
        }
    }
}
