package ir.markazandroid.UniEngine.media.views;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.media.annotations.View;
import ir.markazandroid.UniEngine.media.inits.ViewInitializr;
import ir.markazandroid.UniEngine.object.EFile;
import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;
import ir.markazandroid.UniEngine.persistance.file.entity.MetaDataEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.FileMetaDataDAO;
import ir.markazandroid.UniEngine.service.interfaces.CampaignService;
import ir.markazandroid.UniEngine.service.interfaces.StorageService;
import ir.markazandroid.UniEngine.util.Utils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Ali on 4/22/2019.
 */
@JSON
@View("ir.markazandroid.widget.ScrollTextView")
public class TextView extends BasicView {

    public static final String TYPE_TEXT = "text";

    private String text;
    private String textColor = "0xFF000000";
    private int fontSize = 21;
    private boolean shouldScroll=false;
    private int speed;

    private EFile font;

    public TextView() {
        setType(TYPE_TEXT);
    }


    @JSON
    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    @JSON
    public boolean getShouldScroll() {
        return shouldScroll;
    }

    public void setShouldScroll(boolean shouldScroll) {
        this.shouldScroll = shouldScroll;
    }

    @JSON
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @JSON(classType = JSON.CLASS_TYPE_OBJECT , clazz = EFile.class)
    public EFile getFont() {
        return font;
    }

    public void setFont(EFile font) {
        this.font = font;
    }


    @Override
    public Object getExtrasObject() {
        return this;
    }

    @JSON
    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @JSON
    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }


    @Component(TYPE_TEXT + "_initializr")
    @Scope(BeanDefinition.SCOPE_SINGLETON)
    public static class PlayListViewInitializr implements ViewInitializr, ApplicationListener<StorageService.FileDeletedEvent> {


        private final CampaignService campaignService;
        private final StorageService storageService;
        private final FileMetaDataDAO metaDataDAO;

        public PlayListViewInitializr(CampaignService campaignService, StorageService storageService, FileMetaDataDAO metaDataDAO) {
            this.campaignService = campaignService;
            this.storageService = storageService;
            this.metaDataDAO = metaDataDAO;
        }


        @Override
        public int doInit(BasicView basicView, CampaignEntity campaignEntity) {
            TextView textView = (TextView) basicView;
            if (textView.getFont()!=null){
                storageService.readFile(Utils.decodeString(textView.getFont().geteFileId()));
                metaDataDAO.saveMetaData(new MetaDataEntity(
                        Utils.decodeString(textView.getFont().geteFileId()),
                        StaticImageView.class.getName()  + "@" + campaignEntity.getCampaignId()));
            }
            return 0;
        }


        @Async
        public void onApplicationEvent(StorageService.FileDeletedEvent event) {
            String fileId = storageService.generateFileId(event.getFile());
            metaDataDAO.getFileMetaDatas(fileId)
                    .forEach(metaDataEntity -> {
                        if (metaDataEntity.getReference().startsWith(StaticImageView.class.getName())) {
                            String[] ids = metaDataEntity.getReference().split("@");
                            long campaignId = Long.parseLong(ids[1]);
                            CampaignEntity campaignEntity = campaignService.getCampaignById(campaignId);
                            if (campaignEntity != null) {

                               campaignEntity.getData().getViews().stream()
                                       .filter(basicView -> TYPE_TEXT.equals(basicView.getType()))
                                       .map(basicView -> (TextView) basicView)
                                       .filter(textView -> textView.getFont()!=null
                                               && Utils.encodeString(fileId).equals((textView).getFont().geteFileId()))
                                       .peek(textView -> textView.setFont(null))
                                       .findAny()
                                       .ifPresent(textView -> campaignService.saveOrUpdateCampaign(campaignEntity));
                            }
                            metaDataDAO.deleteMetaData(metaDataEntity);
                        }
                    });
        }

    }
}
