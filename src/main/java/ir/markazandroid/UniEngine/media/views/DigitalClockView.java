package ir.markazandroid.UniEngine.media.views;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.media.annotations.View;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Ali on 4/22/2019.
 */
@JSON
@View("ir.markazandroid.widget.clock.DigitalClock")
public class DigitalClockView extends BasicView {

    public static final String TYPE_DIGITAL_CLOCK= "digitalClock";



    private String textColor;


    public DigitalClockView() {
        setType(TYPE_DIGITAL_CLOCK);
    }


    @Override
    public Object getExtrasObject() {
        return this;
    }


    @JSON
    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }
}
