package ir.markazandroid.UniEngine.media.views;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.media.annotations.View;

/**
 * Created by Ali on 4/22/2019.
 */
@JSON
@View("ir.markazandroid.masteradvertiser.views.camera.CameraWidget")
public class CameraView extends BasicView {

    public static final String TYPE_CAMERA = "camera";

    private int cameraId;

    public CameraView() {
        setType(TYPE_CAMERA);
    }


    @Override
    public Object getExtrasObject() {
        return this;
    }

    @JSON
    public int getCameraId() {
        return cameraId;
    }

    public void setCameraId(int cameraId) {
        this.cameraId = cameraId;
    }
}
