package ir.markazandroid.UniEngine.media.views;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.media.annotations.View;

/**
 * Created by Ali on 4/22/2019.
 */
@JSON
@View("ir.markazandroid.widget.clock.AnalogClock")
public class AnalogClockView extends BasicView {

    public static final String TYPE_ANALOG_CLOCK= "analogClock";


    private String handleColor;


    public AnalogClockView() {
        setType(TYPE_ANALOG_CLOCK);
    }


    @Override
    public Object getExtrasObject() {
        return this;
    }


    @JSON
    public String getHandleColor() {
        return handleColor;
    }

    public void setHandleColor(String handleColor) {
        this.handleColor = handleColor;
    }
}
