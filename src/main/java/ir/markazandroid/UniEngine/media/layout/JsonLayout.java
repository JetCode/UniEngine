package ir.markazandroid.UniEngine.media.layout;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 7/24/2019.
 */

@JSON
public class JsonLayout implements Serializable {


    private ArrayList<Room> rooms;
    private String orientation;
    private Long userId;
    private long layoutId;

    @JSON(classType = JSON.CLASS_TYPE_ARRAY, clazz = Room.class)
    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    @JSON
    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    @JSON
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JSON
    public long getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(long layoutId) {
        this.layoutId = layoutId;
    }

    @JSON
    public static class Room implements Serializable {

        private int x, y, width, height;
        private Room.Name name;
        private String id;

        @JSON
        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        @JSON
        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        @JSON
        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        @JSON
        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        @JSON(classType = JSON.CLASS_TYPE_OBJECT, clazz = Room.Name.class)
        public Room.Name getName() {
            return name;
        }

        public void setName(Room.Name name) {
            this.name = name;
        }

        @JSON
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @JSON
        public static class Name implements Serializable {

            private String text;

            public Name() {

            }

            public Name(String text) {
                this.text = text;
            }

            @JSON
            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }
        }
    }
}
