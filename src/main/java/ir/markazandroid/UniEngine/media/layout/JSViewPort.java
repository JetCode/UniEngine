package ir.markazandroid.UniEngine.media.layout;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

/**
 * Created by Ali on 7/24/2019.
 */
@JSON
public class JSViewPort {

    private int width;
    private int height;
    private Orientation orientation;

    public static JSViewPort getDefaultViewPort(Orientation orientation) {
        if (Orientation.vertical.equals(orientation))
            return getDefaultVerticalViewPort();
        else
            return getDefaultHorizontalViewPort();
    }

    public static JSViewPort getDefaultHorizontalViewPort() {
        return new JSViewPort(150, 100);
    }

    public static JSViewPort getDefaultVerticalViewPort() {
        return new JSViewPort(100, 150);
    }

    public JSViewPort(int width, int height) {
        this.width = width;
        this.height = height;

        if (width > height)
            orientation = Orientation.horizontal;
        else
            orientation = Orientation.vertical;


    }


    public int getScaledWidth(int percent) {
        if (Orientation.horizontal.equals(orientation)) {
            int newWidth = Math.round((percent * width) / 100f);
            if (newWidth > width) return width;
            return newWidth;
        } else return percent;
    }

    public int getScaledHeight(int percent) {
        if (Orientation.vertical.equals(orientation)) {
            int newHeight = Math.round((percent * height) / 100f);
            if (newHeight > height) return height;
            return newHeight;
        } else return percent;
    }


    public int getWidthPercent(int width) {
        return Math.round(((float) width / this.width) * 100);
    }

    public int getHeightPercent(int height) {
        return Math.round(((float) height / this.height) * 100);
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public int getWidth() {
        return width;
    }


    public int getHeight() {
        return height;
    }

    @JSON
    public String getViewPort() {
        return "{0,0," + width + "," + height + "}";
    }

    public void setViewPort(String viewPort) {

    }
}
