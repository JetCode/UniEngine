package ir.markazandroid.UniEngine.media.layout;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Ali on 4/16/2019.
 */
public class LayoutUtils {


    /**
     * exports jsonLayout to send to UI (JS)
     * @param layout
     * @param viewPort
     * @return
     */
    public static JsonLayout buildJsonLayout(Layout layout, JSViewPort viewPort) {
        JsonLayout jsonLayout = new JsonLayout();
        jsonLayout.setOrientation(layout.getOrientation().name());
        jsonLayout.setRooms(layout.getRegions().stream().map(new Function<Region, JsonLayout.Room>() {

            int i = 1;

            @Override
            public JsonLayout.Room apply(Region region) {
                JsonLayout.Room room = new JsonLayout.Room();

                room.setId(region.getId());
                room.setName(new JsonLayout.Room.Name("" + i++));

                room.setX(viewPort.getScaledWidth(region.getLeft().getPercent()));
                room.setY(viewPort.getScaledHeight(region.getTop().getPercent()));

                room.setWidth(viewPort.getScaledWidth(region.getRight().getPercent())
                        - viewPort.getScaledWidth(region.getLeft().getPercent()));
                room.setHeight(viewPort.getScaledHeight(region.getBottom().getPercent())
                        - viewPort.getScaledHeight(region.getTop().getPercent()));

                return room;
            }
        }).collect(Collectors.toCollection(ArrayList::new)));

        return jsonLayout;
    }

    /**
     * builds Layout file from JsonLayout received from UI (JS)
     * @param jsonLayout
     * @param viewPort
     * @return
     */
    public static Layout buildLayout(JsonLayout jsonLayout, JSViewPort viewPort) {
        Layout layout = new Layout();
        layout.setOrientation(viewPort.getOrientation());
        jsonLayout.getRooms()
                .forEach(room -> {
                    Side l = layout.newSide(viewPort.getWidthPercent(room.getX()), Orientation.vertical);
                    Side t = layout.newSide(viewPort.getHeightPercent(room.getY()), Orientation.horizontal);
                    Side r = layout.newSide(viewPort.getWidthPercent(room.getX() + room.getWidth()), Orientation.vertical);
                    Side b = layout.newSide(viewPort.getHeightPercent(room.getY() + room.getHeight()), Orientation.horizontal);
                    layout.newRegion(l, t, r, b);
                });

        return layout;

    }





}
