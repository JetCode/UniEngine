package ir.markazandroid.UniEngine.media.options;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;

@JSON
public abstract class BasicOption implements Serializable {

    private String name;

    @JSON
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
