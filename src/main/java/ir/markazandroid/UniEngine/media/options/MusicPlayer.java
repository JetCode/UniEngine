package ir.markazandroid.UniEngine.media.options;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.media.inits.OptionInitializr;
import ir.markazandroid.UniEngine.media.views.BasicView;
import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;
import ir.markazandroid.UniEngine.persistance.entity.PlayListEntity;
import ir.markazandroid.UniEngine.service.interfaces.CampaignService;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@JSON
public class MusicPlayer extends BasicOption {

    public static final String OPTION_NAME="music_player";

    private PlayListEntity.Data data;
    private long playListId;
    private int duration;

    public MusicPlayer(){
        setName(OPTION_NAME);
    }

    @JSON(classType = JSON.CLASS_TYPE_OBJECT,clazz = PlayListEntity.Data.class)
    public PlayListEntity.Data getData() {
        return data;
    }

    public void setData(PlayListEntity.Data data) {
        this.data = data;
    }

    @JSON
    public long getPlayListId() {
        return playListId;
    }

    public void setPlayListId(long playListId) {
        this.playListId = playListId;
    }

    @JSON
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }


    @Component(OPTION_NAME + "_option_initializr")
    @Scope(BeanDefinition.SCOPE_SINGLETON)
    static class PlayListInitializr implements OptionInitializr {

        private final CampaignService campaignService;

        PlayListInitializr(CampaignService campaignService) {
            this.campaignService = campaignService;
        }

        //TODO check if belongs to user
        @Override
        public int doInit(BasicOption basicOption, CampaignEntity campaignEntity) {
            MusicPlayer musicPlayer = (MusicPlayer) basicOption;
            PlayListEntity playListEntity = campaignService.getPlayListById(musicPlayer.getPlayListId());
            if (playListEntity == null || playListEntity.getUserId()!=campaignEntity.getUserId())
                throw new NotFoundException("PlayList " + musicPlayer.getPlayListId() + " Not Found");

            musicPlayer.setData(playListEntity.getData());
            musicPlayer.setDuration(playListEntity.getDuration());

            return playListEntity.getDuration();
        }
    }

}
