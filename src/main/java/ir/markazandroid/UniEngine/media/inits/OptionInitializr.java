package ir.markazandroid.UniEngine.media.inits;

import ir.markazandroid.UniEngine.media.options.BasicOption;
import ir.markazandroid.UniEngine.media.views.BasicView;

/**
 * Created by Ali on 6/8/2019.
 */
public interface OptionInitializr extends Initializr<BasicOption> {

}
