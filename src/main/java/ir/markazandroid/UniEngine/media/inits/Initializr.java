package ir.markazandroid.UniEngine.media.inits;

import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;

public interface Initializr <T> {
    int doInit(T t, CampaignEntity campaignEntity);
}
