package ir.markazandroid.UniEngine.media.inits;

import ir.markazandroid.UniEngine.media.options.BasicOption;
import ir.markazandroid.UniEngine.media.views.BasicView;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by Ali on 6/8/2019.
 */

@Component
public class Initializrs {

    private final ApplicationContext context;

    @Autowired
    public Initializrs(ApplicationContext context) {
        this.context = context;
    }

    public ViewInitializr getViewInitializrForView(BasicView basicView) {
        try {
            return (ViewInitializr) context.getBean(basicView.getType() + "_initializr");
        } catch (NoSuchBeanDefinitionException ignored) {
            return null;
        }
    }

    public OptionInitializr getOptionInitializrForOption(BasicOption basicOption) {
        try {
            return (OptionInitializr) context.getBean(basicOption.getName() + "_option_initializr");
        } catch (NoSuchBeanDefinitionException ignored) {
            return null;
        }
    }

}
