package ir.markazandroid.UniEngine.timelineUtils;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 8/31/2019.
 */
@JSON
public class Series implements Serializable {

    public static final String DELIMITER_KEEP = "keep";
    public static final String DELIMITER_START_OVER = "start_over";
    public static final String DELIMITER_END = "end";

    /**
     * campaigns by fixed order
     */
    private ArrayList<SeriesCampaign> campaigns;

    /**
     * One of {@value DELIMITER_END}, {@value DELIMITER_START_OVER}, {@value DELIMITER_KEEP}
     */
    private String delimiter;

    /**
    * series starting delay (seconds from 00:00 every day)
     */
    private long from;

    @JSON(classType = JSON.CLASS_TYPE_ARRAY, clazz = SeriesCampaign.class)
    public ArrayList<SeriesCampaign> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(ArrayList<SeriesCampaign> campaigns) {
        this.campaigns = campaigns;
    }

    @JSON
    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    @JSON
    public long getFrom() {
        return from;
    }

    public void setFrom(long from) {
        this.from = from;
    }
}
