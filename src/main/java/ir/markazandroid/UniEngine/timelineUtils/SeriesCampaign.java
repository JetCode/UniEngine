package ir.markazandroid.UniEngine.timelineUtils;

import ir.markazandroid.UniEngine.JSONParser.annotations.JSON;

import java.io.Serializable;

/**
 * Created by Ali on 8/31/2019.
 *
 * provides needed details for a campaign in series
 */
@JSON
public class SeriesCampaign implements Serializable {

    private long campaignId;
    private int duration;
    private String campaignName;


    @JSON
    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    @JSON
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @JSON
    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }
}
