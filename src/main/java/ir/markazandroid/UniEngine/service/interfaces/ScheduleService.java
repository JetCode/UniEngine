package ir.markazandroid.UniEngine.service.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.ScheduleEntity;
import ir.markazandroid.UniEngine.persistance.entity.ScheduleHasTimeLineEntity;
import ir.markazandroid.UniEngine.persistance.entity.TimeLineEntity;

import java.util.List;

/**
 * Created by Ali on 9/1/2019.
 */
public interface ScheduleService {


    List<ScheduleEntity> getUserSchedules(long userId);

    ScheduleEntity getDetailedSchedule(int scheduleId);

    ScheduleEntity getScheduleById(int scheduleId);

    /**
     * saves the schedule
     * @param scheduleEntity
     * @return
     */
    ScheduleEntity newSchedule(ScheduleEntity scheduleEntity);

    /**
     * just updates schedule name
     * @param ScheduleId
     * @param name
     */
    void updateScheduleName(int ScheduleId, String name);

    void deleteSchedule(int scheduleId);

    TimeLineEntity getTimelineById(long timelineId);

    List<TimeLineEntity> getUserTimelines(long userId);

    List<TimeLineEntity> getUserTimelinesWithSeries(long userId);

    void saveOrUpdateTimeline(TimeLineEntity timelineEntity);

    void deleteTimeline(long timelineId);

    ScheduleHasTimeLineEntity getShtById(long shtId);

    /**
     * saves the schedule
     * @param sht
     */
    void setTime(ScheduleHasTimeLineEntity sht);

    void deleteSHT(long shtId);


}
