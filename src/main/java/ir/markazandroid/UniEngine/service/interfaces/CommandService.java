package ir.markazandroid.UniEngine.service.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.CommandEntity;

import java.util.List;

/**
 * Created by Ali on 2/6/2019.
 */
public interface CommandService {

    void saveCommand(CommandEntity commandEntity);

    void updateCommand(CommandEntity commandEntity, String... fields);

    //List<CommandEntity> getPhoneCommands(int deviceId);
    //List<CommandEntity> getPhoneUnReceivedCommands(int deviceId);
    List<CommandEntity> getUnReceivedCommands();

    CommandEntity getCommandByMessageId(String messageId);
}
