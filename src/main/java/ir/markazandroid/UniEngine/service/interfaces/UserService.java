package ir.markazandroid.UniEngine.service.interfaces;

import ir.markazandroid.UniEngine.exception.VerificationCodeAlreadySentException;
import ir.markazandroid.UniEngine.exception.VerificationCodeExpiredException;
import ir.markazandroid.UniEngine.exception.VerificationCodeIncorrectException;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;

import java.util.List;
import java.util.Locale;

/**
 * Created by Ali on 30/01/2018.
 * not all of the methods are used (they are pasted from advertiserv4)
 */
public interface UserService {
    void register(UserEntity userEntity);

    boolean sendEmailVerificationCode(long userId, Locale locale) throws VerificationCodeAlreadySentException;

    boolean checkVerificationCode(long userId);

    UserEntity getUserWithPrivileges(long userId);


    void verifyUser(String code, long userId) throws VerificationCodeIncorrectException, VerificationCodeExpiredException;

    boolean sendPasswordResetSms(String phone, String username);

    boolean checkPasswordResetSms(String username);

    void verifyPasswordResetToken(long userId, String token);

    void changeUserPassword(long userId, String password);

    UserEntity getUserByUserId(long userId);

    UserEntity getUserByEmail(String email);

    UserEntity getUserByMeliCode(String meliCode);


    List<UserEntity> getUsers(int offset, int limit);

    long getUsersCount();

    void changeUserEmail(long userId, String email);

    void changeUserPhone(UserEntity userEntity, String phone);

    /**
     * firstname lastname
     * @param userEntity
     */
    void changeUserDetails(UserEntity userEntity);

    void initUserParams(UserEntity userEntity);

    List<DeviceEntity> getUserOnlineDevices(long userId);

}
