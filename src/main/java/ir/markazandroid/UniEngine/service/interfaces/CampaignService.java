package ir.markazandroid.UniEngine.service.interfaces;

import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;
import ir.markazandroid.UniEngine.persistance.entity.LayoutEntity;
import ir.markazandroid.UniEngine.persistance.entity.PlayListEntity;

import java.util.List;

/**
 * Created by Ali on 4/14/2019.
 */
public interface CampaignService {

    void saveOrUpdatePlayList(PlayListEntity playListEntity);

    List<PlayListEntity> getUserPlayLists(long userId);

    PlayListEntity getPlayListById(long playListId);

    void deletePlayListById(long playListId);

    /**
     * returns the campaign - creates one if it doesnt exists
     * @param userId
     * @param campaignId
     * @return
     */
    CampaignEntity obtainCampaign(long userId, long campaignId);

    /**
     * inits the views and options - also makes the android data
     * @param campaignEntity
     */
    void initCampaignData(CampaignEntity campaignEntity);

    /**
     * persists the obtained campaign see {@link #obtainCampaign} and initialized see {@link #initCampaignData} campaign
     * @param campaignEntity
     */
    void saveOrUpdateCampaign(CampaignEntity campaignEntity);

    void deleteCampaign(long campaignId);

    CampaignEntity getCampaignById(long campaignId);

    List<CampaignEntity> getUserCampaigns(long userId);

    long getUserCampaignCount(long userId);

    void saveLayout(LayoutEntity layoutEntity);

    LayoutEntity getLayoutById(long layoutId);

    /**
     * user created layouts plus free ones
     * @param userId
     * @param orientation
     * @return
     */
    List<LayoutEntity> getUserAvailableLayouts(long userId, String orientation);

    /**
     * just user created layouts
     * @param userId
     * @return
     */
    List<LayoutEntity> getUserLayouts(long userId);

    void deleteLayout(long layoutId);


}
