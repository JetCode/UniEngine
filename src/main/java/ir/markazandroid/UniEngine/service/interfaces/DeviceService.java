package ir.markazandroid.UniEngine.service.interfaces;

import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceOnline;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceGroupEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceModelEntity;

import java.util.List;

/**
 * Created by Ali on 6/9/2019.
 */
public interface DeviceService {

    void registerDevice(DeviceEntity deviceEntity);

    DeviceEntity loginDevice(String UUID);

    DeviceEntity getDeviceByUUID(String UUID);

    DeviceEntity getDeviceById(long deviceId);

    DeviceEntity getDeviceByName(String deviceName);

    /**
     * for user to add device in his panel
     * @param userId
     * @param name device name
     * @param pass device pass
     * @return the owned device
     * @throws NotFoundException
     */
    DeviceEntity ownDevice(long userId, String name, String pass) throws NotFoundException;

    void disownDevice(long deviceId) throws NotFoundException;

    /**
     * hold the status of user online devices
     * @param deviceOnline
     */
    void saveDeviceOnline(DeviceOnline deviceOnline);

    DeviceOnline getDeviceOnline(long deviceId);

    /**
     * device data such and sensor data
     * @param deviceData
     */
    void saveDeviceData(DeviceData deviceData);

    DeviceData getLatestDeviceData(long deviceId);

    List<DeviceData> getLatestDeviceData(long deviceId, long lateSeconds, String... fields);

    /**
     * returns the device data recorded in a whole day
     * @param deviceId
     * @param dayTimestamp
     * @param fields
     * @return
     */
    List<DeviceData> getDayFullData(long deviceId, long dayTimestamp, String... fields);

    /**
     * user owned devices
     * @param userId
     * @return
     */
    List<DeviceEntity> getUserDevices(long userId);

    /**
     * devices submited with user meli code
     * @param userId
     * @return
     */
    List<DeviceEntity> getDevicesMadeForUser(long userId);

    void saveDeviceGroup(DeviceGroupEntity deviceGroupEntity);

    DeviceGroupEntity getDeviceGroupById(long deviceGroupId);

    List<DeviceGroupEntity> getUserDeviceGroups(long userId);

    void addToGroup(long groupId, long deviceId);

    void removeFromGroup(long groupId, long deviceId);

    /**
     * overrides all of group details such and devices and name
     * @param deviceGroupEntity
     */
    void updateGroup(DeviceGroupEntity deviceGroupEntity);

    void deleteGroup(long groupId);

    void refreshAllUserDeviceSchedules(long userId);

    /**
     * forces the device to refresh its schedule
     * @param deviceId
     * @param scheduleId
     */
    void refreshActiveAdvertisingSchedule(long deviceId, Integer scheduleId);

    /**
     * for production line
     * @return
     */
    List<DeviceModelEntity> getDeviceModels();


    /**
     * invalidates device session forces the device to login again
     * @param deviceId
     */
    void logoutDevice(long deviceId);



}
