package ir.markazandroid.UniEngine.service;

import ir.markazandroid.UniEngine.UniEngineApplication;
import ir.markazandroid.UniEngine.conf.session.SessionManager;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.exception.PhoneNameExistsException;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceData;
import ir.markazandroid.UniEngine.persistance.cassandra.entity.DeviceOnline;
import ir.markazandroid.UniEngine.persistance.cassandra.interfaces.DeviceDataDAO;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceGroupEntity;
import ir.markazandroid.UniEngine.persistance.entity.DeviceModelEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.DeviceDAO;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import ir.markazandroid.UniEngine.service.interfaces.UserService;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Ali on 6/10/2019.
 */
@Service
public class DeviceServiceImp implements DeviceService {

    private final DeviceDAO deviceDAO;
    private final SessionManager sessionManager;
    private final Random random;
    private final DeviceDataDAO deviceDataDAO;
    private final UserService userService;

    public DeviceServiceImp(DeviceDAO deviceDAO, SessionManager sessionManager, Random random, DeviceDataDAO deviceDataDAO, UserService userService) {
        this.deviceDAO = deviceDAO;
        this.sessionManager = sessionManager;
        this.random = random;
        this.deviceDataDAO = deviceDataDAO;
        this.userService = userService;
    }

    @Transactional
    @Override
    public void registerDevice(DeviceEntity deviceEntity) {
        if (deviceDAO.getDeviceByName(deviceEntity.getName()) != null)
            throw new PhoneNameExistsException();

        deviceEntity.setUuid(UUID.randomUUID().toString());
        deviceEntity.setPassKey(getSaltString());
        deviceEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
        deviceEntity.setStatus(DeviceEntity.STATUS_NOT_ASSIGNED);
        deviceEntity.setRoleId(UniEngineApplication.ROLE_DEVICE.getRoleId());

        deviceDAO.saveDevice(deviceEntity);
    }

    @Transactional
    @Override
    public DeviceEntity loginDevice(String UUID) {
        DeviceEntity deviceEntity = deviceDAO.getDeviceByUUID(UUID);
        if (deviceEntity == null) return null;
        deviceEntity.makePrivileges();
        deviceEntity.setLastUpdateVersion(System.currentTimeMillis());
        return deviceEntity;
    }

    @Override
    public DeviceEntity getDeviceByUUID(String UUID) {
        return deviceDAO.getDeviceByUUID(UUID);
    }

    @Override
    public DeviceEntity getDeviceById(long deviceId) {
        return deviceDAO.getDeviceById(deviceId);
    }

    @Override
    public DeviceEntity getDeviceByName(String deviceName) {
        return deviceDAO.getDeviceByName(deviceName);
    }

    @Transactional
    @Override
    public DeviceEntity ownDevice(long userId, String name, String pass) throws NotFoundException {
        DeviceEntity deviceEntity = deviceDAO.getDeviceByName(name);
        if (deviceEntity == null || !deviceEntity.getPassKey().equals(pass) || deviceEntity.getUserId() != null)
            throw new NotFoundException();

        deviceEntity.setUserId(userId);
        deviceEntity.setAssignDate(new Date(System.currentTimeMillis()));

        deviceDAO.updateDevice(deviceEntity);

        //update user status
        UserEntity userEntity = userService.getUserByUserId(userId);
        if (userEntity.getStatus() == UserEntity.STATUS_VERIFIED_HAS_NO_DEVICE) {
            userEntity.setStatus(UserEntity.STATUS_VERIFIED_HAS_DEVICE);
            if (userEntity.getRoleId() == UniEngineApplication.ROLE_USER.getRoleId())
                userEntity.setRoleId(UniEngineApplication.ROLE_USER_BASIC.getRoleId());
        }

        sessionManager.updateMainPrincipal(DeviceEntity.class, deviceEntity, principal -> {
            principal.setUserId(userId);
            UserEntity owner = sessionManager.getMainPrincipal(userEntity);
            if (owner == null) owner = userEntity;
            principal.setUser(owner);
            principal.makePrivileges();
        });
        sessionManager.updateMainPrincipal(UserEntity.class, userEntity,
                principal -> {
                    principal.getParams().setDeviceCount(principal.getParams().getDeviceCount() + 1);
                    if (principal.getStatus() == UserEntity.STATUS_VERIFIED_HAS_NO_DEVICE) {
                        principal.setStatus(UserEntity.STATUS_VERIFIED_HAS_DEVICE);

                        if (principal.getRoleId() == UniEngineApplication.ROLE_USER.getRoleId()) {
                            principal.setRoleId(UniEngineApplication.ROLE_USER_BASIC.getRoleId());
                            principal.setRole(UniEngineApplication.ROLE_USER_BASIC);
                            principal.makePrivileges();
                        }
                    }
                });

        return deviceEntity;
    }

    @Transactional
    @Override
    public void disownDevice(long deviceId) throws NotFoundException {
        throw new NotImplementedException();
    }

    @Override
    public void saveDeviceOnline(DeviceOnline deviceOnline) {
        deviceDataDAO.saveDeviceOnline(deviceOnline);
    }

    @Override
    public DeviceOnline getDeviceOnline(long deviceId) {
        return deviceDataDAO.getDeviceOnline(deviceId);
    }

    @Override
    public void saveDeviceData(DeviceData deviceData) {
        deviceDataDAO.saveDeviceData(deviceData);
    }

    @Override
    public DeviceData getLatestDeviceData(long deviceId) {
        return deviceDataDAO.getLatestDeviceData(deviceId);
    }

    @Override
    public List<DeviceData> getLatestDeviceData(long deviceId, long lateSeconds, String... fields) {
        return deviceDataDAO.getLatestDeviceData(deviceId, lateSeconds, fields);
    }

    @Override
    public List<DeviceData> getDayFullData(long deviceId, long dayTimestamp, String... fields) {
        return deviceDataDAO.getDayFullData(deviceId, dayTimestamp, fields);
    }

    @Override
    public List<DeviceEntity> getUserDevices(long userId) {
        return deviceDAO.getUserDevices(userId);
    }

    @Override
    public List<DeviceEntity> getDevicesMadeForUser(long userId) {
        UserEntity userEntity = userService.getUserByUserId(userId);
        return deviceDAO.getNotAssignedDevicesByOwnerCode(userEntity.getMeliCode());
    }

    @Transactional
    @Override
    public void saveDeviceGroup(DeviceGroupEntity deviceGroupEntity) {
        deviceDAO.saveDeviceGroup(deviceGroupEntity);
    }

    @Override
    public DeviceGroupEntity getDeviceGroupById(long deviceGroupId) {
        return deviceDAO.getDeviceGroupById(deviceGroupId);
    }

    @Override
    public List<DeviceGroupEntity> getUserDeviceGroups(long userId) {
        return deviceDAO.getUserDeviceGroups(userId);
    }


    //TODO update statement
    @Transactional
    @Override
    public void addToGroup(long groupId, long deviceId) {
        DeviceGroupEntity group = deviceDAO.getDeviceGroupById(groupId);
        DeviceEntity device = deviceDAO.getDeviceById(deviceId);
        group.getDevices().add(device);
    }

    //TODO update statement
    @Transactional
    @Override
    public void removeFromGroup(long groupId, long deviceId) {
        DeviceGroupEntity group = deviceDAO.getDeviceGroupById(groupId);
        DeviceEntity device = deviceDAO.getDeviceById(deviceId);
        group.getDevices().remove(device);
    }

    @Transactional
    @Override
    public void updateGroup(DeviceGroupEntity deviceGroupEntity) {
        DeviceGroupEntity group = deviceDAO.getDeviceGroupById(deviceGroupEntity.getDeviceGroupId());
        group.setGroupName(deviceGroupEntity.getGroupName());
        group.getDevices().clear();
        if (deviceGroupEntity.getDevices() != null) {
            deviceGroupEntity.getDevices().stream()
                    .mapToLong(DeviceEntity::getDeviceId)
                    .forEach(value -> {
                        DeviceEntity deviceEntity = deviceDAO.getDeviceById(value);
                        if (deviceEntity.getUserId() == null || deviceEntity.getUserId() != deviceGroupEntity.getUserId())
                            throw new NotFoundException();
                        group.getDevices().add(deviceEntity);
                    });
        }
    }


    @Transactional
    @Override
    public void deleteGroup(long groupId) {
        deviceDAO.deleteDeviceGroup(groupId);
    }

    @Override
    public void refreshAllUserDeviceSchedules(long userId) {
        userService.getUserOnlineDevices(userId).parallelStream()
                .forEach(deviceEntity -> deviceEntity.setLastUpdateVersion(System.currentTimeMillis()));
    }

    @Transactional
    @Override
    public void refreshActiveAdvertisingSchedule(long deviceId, Integer scheduleId) {
        DeviceEntity managedDeviceEntity = deviceDAO.getDeviceById(deviceId);
        if (managedDeviceEntity != null) {
            managedDeviceEntity.setAdvertiserScheduleId(scheduleId);

            sessionManager.updateMainPrincipal(DeviceEntity.class, managedDeviceEntity
                    , deviceEntity -> {
                        deviceEntity.setLastUpdateVersion(System.currentTimeMillis());
                        deviceEntity.setAdvertiserScheduleId(scheduleId);
                    });
        }

    }

    @Override
    public List<DeviceModelEntity> getDeviceModels() {
        return deviceDAO.getDeviceModels();
    }

    @Override
    public void logoutDevice(long deviceId) {
        sessionManager.removeAllSessions(() -> DeviceEntity.buildUniqueKey(deviceId));
    }

    private String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        int i = 0;
        while (salt.length() < 14) {// length of the random string.
            i++;
            if (((i) % 5) == 0 && i != 1) {
                salt.append("-");
                continue;
            }
            int index = (int) (random.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

}
