package ir.markazandroid.UniEngine.service;

import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.ScheduleEntity;
import ir.markazandroid.UniEngine.persistance.entity.ScheduleHasTimeLineEntity;
import ir.markazandroid.UniEngine.persistance.entity.TimeLineEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.ScheduleDAO;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import ir.markazandroid.UniEngine.service.interfaces.ScheduleService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ali on 9/1/2019.
 */
@Service
public class ScheduleServiceImp implements ScheduleService {

    private final ScheduleDAO scheduleDAO;
    private final DeviceService deviceService;

    public ScheduleServiceImp(ScheduleDAO scheduleDAO, DeviceService deviceService) {
        this.scheduleDAO = scheduleDAO;
        this.deviceService = deviceService;
    }

    @Override
    public List<ScheduleEntity> getUserSchedules(long userId) {
        return scheduleDAO.getUserSchedules(userId);
    }

    @Override
    public ScheduleEntity getDetailedSchedule(int scheduleId) {
        return scheduleDAO.getFullSchedule(scheduleId);
    }

    @Override
    public ScheduleEntity getScheduleById(int scheduleId) {
        return scheduleDAO.getScheduleById(scheduleId);
    }

    @Transactional
    @Override
    public ScheduleEntity newSchedule(ScheduleEntity scheduleEntity) {
        scheduleEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
        scheduleDAO.saveSchedule(scheduleEntity);
        return scheduleEntity;
    }

    @Transactional
    @Override
    public void updateScheduleName(int scheduleId, String name) {
        ScheduleEntity scheduleEntity = scheduleDAO.getScheduleById(scheduleId);
        scheduleEntity.setName(name);
    }

    @Transactional
    @Override
    public void deleteSchedule(int scheduleId) {
        ScheduleEntity scheduleEntity = scheduleDAO.getScheduleById(scheduleId);
        Collection<DeviceEntity> devices = scheduleEntity.getDevices();
        scheduleDAO.deleteSchedule(scheduleEntity);
        devices.forEach(deviceEntity ->
                deviceService.refreshActiveAdvertisingSchedule(deviceEntity.getDeviceId(), null));

    }

    @Override
    public TimeLineEntity getTimelineById(long timelineId) {
        return scheduleDAO.getTimelineById(timelineId);
    }

    @Override
    public List<TimeLineEntity> getUserTimelines(long userId) {
        return scheduleDAO.getUserTimelines(userId);
    }

    @Override
    public List<TimeLineEntity> getUserTimelinesWithSeries(long userId) {
        return scheduleDAO.getUserTimelinesWithSeries(userId);
    }

    @Transactional
    @Override
    public void saveOrUpdateTimeline(TimeLineEntity timelineEntity) {
        boolean isUpdate = timelineEntity.getTimelineId() != 0;

        if (isUpdate) {
            scheduleDAO.detach(timelineEntity);
            TimeLineEntity managedEntity = scheduleDAO.getTimelineById(timelineEntity.getTimelineId());
            if (managedEntity == null) throw new NotFoundException();

            managedEntity.setSeries(timelineEntity.getSeries());
            managedEntity.setName(timelineEntity.getName());

            scheduleDAO.updateTimeline(managedEntity);

            managedEntity.getShts()
                    .forEach(scheduleHasTimeLineEntity -> scheduleHasTimeLineEntity.getSchedule().getDevices()
                            .forEach(deviceEntity -> deviceService.refreshActiveAdvertisingSchedule(deviceEntity.getDeviceId(), scheduleHasTimeLineEntity.getSchedule().getScheduleId())));

        } else {
            timelineEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
            scheduleDAO.saveTimeline(timelineEntity);
        }
    }

    @Transactional
    @Override
    public void deleteTimeline(long timelineId) {
        TimeLineEntity timeLineEntity = scheduleDAO.getTimelineById(timelineId);
        timeLineEntity.getShts()
                .forEach(scheduleHasTimeLineEntity -> deleteSHT(scheduleHasTimeLineEntity.getShtId()));
        scheduleDAO.deleteTimeline(timeLineEntity);
    }

    @Override
    public ScheduleHasTimeLineEntity getShtById(long shtId) {
        return scheduleDAO.getScheduleHasTimelineBySHTId(shtId);
    }

    @Transactional
    @Override
    public void setTime(ScheduleHasTimeLineEntity sht) {
        boolean isUpdate = sht.getShtId() != 0;

        if (isUpdate) {
            ScheduleHasTimeLineEntity managedEntity = scheduleDAO.getScheduleHasTimelineBySHTId(sht.getShtId());
            if (managedEntity == null) throw new NotFoundException();

            managedEntity.setCron(sht.getCron());
            managedEntity.setCroned((byte) (sht.getCron() != null && !sht.getCron().isEmpty() ? 1 : 0));
            managedEntity.setDates(sht.getDates());

            scheduleDAO.updateScheduleHasTimeline(managedEntity);
            sht = managedEntity;
        } else {
            sht.setCroned((byte) (sht.getCron() != null && !sht.getCron().isEmpty() ? 1 : 0));
            scheduleDAO.saveScheduleHasTimeline(sht);
        }

        ScheduleHasTimeLineEntity finalSht = sht;
        sht.getSchedule().getDevices().forEach(deviceEntity -> deviceService.refreshActiveAdvertisingSchedule(deviceEntity.getDeviceId(), finalSht.getSchedule().getScheduleId()));


    }

    @Transactional
    @Override
    public void deleteSHT(long shtId) {
        ScheduleHasTimeLineEntity managedEntity = scheduleDAO.getScheduleHasTimelineBySHTId(shtId);
        ScheduleEntity scheduleEntity = managedEntity.getSchedule();

        scheduleDAO.deleteScheduleHasTimeline(managedEntity);

        scheduleEntity.getDevices().forEach(deviceEntity -> deviceService.refreshActiveAdvertisingSchedule(deviceEntity.getDeviceId(), scheduleEntity.getScheduleId()));
    }
}
