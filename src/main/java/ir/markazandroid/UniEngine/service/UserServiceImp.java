package ir.markazandroid.UniEngine.service;

import ir.markazandroid.UniEngine.UniEngineApplication;
import ir.markazandroid.UniEngine.conf.session.SessionManager;
import ir.markazandroid.UniEngine.exception.*;
import ir.markazandroid.UniEngine.object.Token;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.DeviceDAO;
import ir.markazandroid.UniEngine.persistance.interfaces.TokenRepository;
import ir.markazandroid.UniEngine.persistance.interfaces.UserDao;
import ir.markazandroid.UniEngine.service.interfaces.UserService;
import ir.markazandroid.UniEngine.sms.KavenegarApi;
import ir.markazandroid.UniEngine.util.EmailUtils;
import ir.markazandroid.UniEngine.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Ali on 30/01/2018.
 */
@Service
public class UserServiceImp implements UserService {

    private final UserDao userDao;
    private final DeviceDAO deviceDAO;
    private final TokenRepository tokenRepository;
    private final Random random;
    private final KavenegarApi kavenegarApi;
    private final BCryptPasswordEncoder passwordEncoder;
    private final EmailUtils emailUtils;
    private final MessageSource messageSource;
    private final SessionManager sessionManager;


    @Autowired
    public UserServiceImp(UserDao userDao, DeviceDAO deviceDAO, TokenRepository tokenRepository,
                          Random random, KavenegarApi kavenegarApi,
                          BCryptPasswordEncoder passwordEncoder,
                          EmailUtils emailUtils, MessageSource messageSource,
                          SessionManager sessionManager) {
        this.userDao = userDao;
        this.deviceDAO = deviceDAO;
        this.tokenRepository = tokenRepository;
        this.random = random;
        this.kavenegarApi = kavenegarApi;
        this.passwordEncoder = passwordEncoder;
        this.emailUtils = emailUtils;
        this.messageSource = messageSource;
        this.sessionManager = sessionManager;
    }

    @Transactional
    @Override
    public void register(UserEntity userEntity) {
        userEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
        userEntity.setStatus(UserEntity.STATUS_NOT_VERIFIED);
        //userEntity.setRoleId(1);
        userDao.saveUser(userEntity);
        sendEmailVerificationCode(userEntity.getUserId(), new Locale("fa", ""));
    }


    @Override
    public boolean sendEmailVerificationCode(long userId, Locale locale) throws VerificationCodeAlreadySentException {
        UserEntity userEntity = userDao.getUserByUserId(userId);
        if (userEntity == null) throw new NotFoundException();

        Token token = tokenRepository.getTokenByUsername("user_" + userId);
        if (token != null && token.getTime() + 60000 > System.currentTimeMillis())
            throw new VerificationCodeAlreadySentException((60000 - (System.currentTimeMillis() - token.getTime())) / 1000);

        String ts = (random.nextInt(899999) + 100000) + "";
        token = new Token(ts, System.currentTimeMillis());
        boolean sent = true;
        String message = messageSource.getMessage("account_verification_code", null, "Verification URL: ", locale) +
                messageSource.getMessage("url_prefix", null, "", locale) + ts;
        String title = messageSource.getMessage("account_verification", null, "Unitech Account Verification ", locale);
        try {
            emailUtils.sendSimpleMessage(userEntity.getEmail(), title, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sent) tokenRepository.add("user_" + userId, token);
        return sent;
    }

    @Override
    public boolean checkVerificationCode(long userId) {
        Token token = tokenRepository.getTokenByUsername("user_" + userId);
        if (token != null && token.getTime() + 60000 > System.currentTimeMillis())
            throw new VerificationCodeAlreadySentException((60000 - (System.currentTimeMillis() - token.getTime())) / 1000);

        return token != null;
    }

    @Transactional
    @Override
    public UserEntity getUserWithPrivileges(long userId) {
        UserEntity userEntity = getUserByUserId(userId);
        userEntity.makePrivileges();
        initUserParams(userEntity);
        return userEntity;
    }

    @Transactional
    @Override
    public void verifyUser(String code, long userId) throws VerificationCodeIncorrectException, VerificationCodeExpiredException {
        Token token = tokenRepository.getTokenByUsername("user_" + userId);

        if (token == null || !token.getToken().equals(code))
            throw new VerificationCodeIncorrectException();
        if (token.getTime() >= System.currentTimeMillis() + 20 * 60 * 1000)
            throw new VerificationCodeExpiredException();

        UserEntity userEntity = userDao.getUserByUserId(userId);
        if (userEntity.getStatus() != UserEntity.STATUS_NOT_VERIFIED)
            throw new RuntimeException("User is not in \"STATUS_NOT_VERIFIED\" State! You Kidding me?!");

        userEntity.setStatus(UserEntity.STATUS_VERIFIED_HAS_NO_DEVICE);
        userEntity.setRoleId(UniEngineApplication.ROLE_USER.getRoleId());

        tokenRepository.remove("user_" + userEntity.getUserId());

        sessionManager.updateMainPrincipal(UserEntity.class, userEntity, principal -> {
            principal.setStatus(UserEntity.STATUS_VERIFIED_HAS_NO_DEVICE);
            principal.setRoleId(UniEngineApplication.ROLE_USER.getRoleId());
            principal.setRole(UniEngineApplication.ROLE_USER);
            principal.makePrivileges();
        });

    }

    @Override
    public boolean sendPasswordResetSms(String phone, String username) {

        UserEntity userEntity = userDao.getUserByEmail(username, "userId", "phone", "username");
        if (userEntity == null || !userEntity.getPhone().equals(phone))
            throw new UsernameNotFoundException("user not found");

        Token token = tokenRepository.getTokenByUsername("user_password_reset_" + userEntity.getUserId());
        if (token != null && token.getTime() + 60 * 1000 > System.currentTimeMillis())
            throw new ResetPasswordSmsAlreadySentException((60 * 1000 - (System.currentTimeMillis() - token.getTime())) / 1000);

        String ts = Utils.getSaltString(random, 10);
        token = new Token(ts, System.currentTimeMillis());
        boolean sent = false;
        try {
            sent = kavenegarApi.send("10000004044404", phone, "برای تغییر گذرواژه روی لینک زیر کلیک کنید.\n"
                    + "http://89.42.210.32/UniEnginev2/web/user/authentication/vrpt/" + userEntity.getUserId() + "/" + ts).getStatus() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sent) tokenRepository.add("user_password_reset_" + userEntity.getUserId(), token);
        return sent;
    }

    @Override
    public boolean checkPasswordResetSms(String username) {
        // Token token=tokenRepository.getTokenByUsername("user_password_reset_"+username);
        // if (token!=null && token.getTime()+60*1000>System.currentTimeMillis())
        //     throw new ResetPasswordSmsAlreadySentException((60*1000-(System.currentTimeMillis()-token.getTime()))/1000);

        //  return token != null;
        return true;
    }

    @Override
    public void verifyPasswordResetToken(long userId, String tokenString) {
        Token token = tokenRepository.getTokenByUsername("user_password_reset_" + userId);

        if (token == null || !token.getToken().equals(tokenString))
            throw new ResetPasswordIncorrectException();
        if (token.getTime() >= System.currentTimeMillis() + 20 * 60 * 1000)
            throw new ResetPasswordSmsExpiredException();

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(userId);

        Authentication auth = new UsernamePasswordAuthenticationToken(
                userEntity, null, Collections.singletonList(
                new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
        SecurityContextHolder.getContext().setAuthentication(auth);

        tokenRepository.remove("user_password_reset_" + userId);
    }

    @Transactional
    @Override
    public void changeUserPassword(long userId, String password) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(userId);
        userEntity.setPassword(password);
        userDao.updateUser(userEntity, "password");
    }

    @Override
    public UserEntity getUserByUserId(long userId) {
        return userDao.getUserByUserId(userId);
    }

    @Override
    public UserEntity getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    @Override
    public UserEntity getUserByMeliCode(String meliCode) {
        return userDao.getUserByMeliCode(meliCode);
    }

    @Override
    public List<UserEntity> getUsers(int offset, int limit) {
        return userDao.getUsers(offset, limit);
    }

    @Override
    public long getUsersCount() {
        return userDao.getUsersCount();
    }

    @Transactional
    @Override
    public void changeUserEmail(long userId, String email) {
        UserEntity userEntity = userDao.getUserByUserId(userId);
        if (userEntity.getStatus() == UserEntity.STATUS_NOT_VERIFIED && userEntity.getRole() == null) {
            userEntity.setEmail(email);
            sessionManager.updateMainPrincipal(UserEntity.class, userEntity, principal -> {
                principal.setEmail(email);
            });
        } else throw new RuntimeException("This User Cannot Change Email");
    }

    @Transactional
    @Override
    public void changeUserPhone(UserEntity userEntity, String phone) {
        UserEntity user = new UserEntity();
        user.setUserId(userEntity.getUserId());
        user.setPhone(phone);
        user.setStatus(UserEntity.STATUS_NOT_VERIFIED);
        userDao.updateUser(user, "phone", "status");
        userEntity.setPhone(phone);
        userEntity.setStatus(UserEntity.STATUS_NOT_VERIFIED);
    }

    @Transactional
    @Override
    public void changeUserDetails(UserEntity userEntity) {
        userDao.updateUser(userEntity, "firstName", "lastName");
    }

    @Override
    public void initUserParams(UserEntity userEntity) {
        userEntity.setParams(new UserEntity.Params());
        userEntity.getParams().setDeviceCount(deviceDAO.getUserDevices(userEntity.getUserId()).size());
    }

    //TODO avoid loop over all sessions and increase performance
    @Override
    public List<DeviceEntity> getUserOnlineDevices(long userId) {
        return sessionManager.getMainPrincipals(DeviceEntity.class,
                deviceEntity -> deviceEntity.getUserId() != null && deviceEntity.getUserId() == userId && Utils.isDeviceOnline(deviceEntity));
    }


}
