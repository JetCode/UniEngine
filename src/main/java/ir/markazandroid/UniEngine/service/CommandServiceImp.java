package ir.markazandroid.UniEngine.service;

import ir.markazandroid.UniEngine.persistance.entity.CommandEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.CommandDAO;
import ir.markazandroid.UniEngine.service.interfaces.CommandService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Ali on 2/6/2019.
 */
@Service
public class CommandServiceImp implements CommandService {

    private final CommandDAO commandDAO;

    public CommandServiceImp(CommandDAO commandDAO) {
        this.commandDAO = commandDAO;
    }

    @Transactional
    @Override
    public void saveCommand(CommandEntity commandEntity) {
        commandDAO.saveCommand(commandEntity);
    }

    @Transactional
    @Override
    public void updateCommand(CommandEntity commandEntity, String... fields) {
        commandDAO.updateCommand(commandEntity, fields);
    }

    @Override
    public List<CommandEntity> getUnReceivedCommands() {
        return commandDAO.getUnReceivedCommands();
    }

    @Transactional
    @Override
    public CommandEntity getCommandByMessageId(String messageId) {
        return commandDAO.getCommandByMessageId(messageId);
    }
}
