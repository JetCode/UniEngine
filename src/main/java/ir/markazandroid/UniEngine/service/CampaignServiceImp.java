package ir.markazandroid.UniEngine.service;

import ir.markazandroid.UniEngine.exception.ForbiddenException;
import ir.markazandroid.UniEngine.exception.NotFoundException;
import ir.markazandroid.UniEngine.media.content.ContentHolder;
import ir.markazandroid.UniEngine.media.inits.OptionInitializr;
import ir.markazandroid.UniEngine.media.layout.Layout;
import ir.markazandroid.UniEngine.media.layout.Region;
import ir.markazandroid.UniEngine.media.options.BasicOption;
import ir.markazandroid.UniEngine.media.views.PlayListView;
import ir.markazandroid.UniEngine.media.inits.ViewInitializr;
import ir.markazandroid.UniEngine.media.inits.Initializrs;
import ir.markazandroid.UniEngine.persistance.entity.CampaignEntity;
import ir.markazandroid.UniEngine.persistance.entity.LayoutEntity;
import ir.markazandroid.UniEngine.persistance.entity.PlayListEntity;
import ir.markazandroid.UniEngine.persistance.file.entity.MetaDataEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.CampaignDAO;
import ir.markazandroid.UniEngine.persistance.interfaces.FileMetaDataDAO;
import ir.markazandroid.UniEngine.service.interfaces.CampaignService;
import ir.markazandroid.UniEngine.service.interfaces.DeviceService;
import ir.markazandroid.UniEngine.service.interfaces.ScheduleService;
import ir.markazandroid.UniEngine.service.interfaces.StorageService;
import ir.markazandroid.UniEngine.timelineUtils.Series;
import ir.markazandroid.UniEngine.timelineUtils.SeriesCampaign;
import ir.markazandroid.UniEngine.util.Utils;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by Ali on 4/14/2019.
 */
@Service
public class CampaignServiceImp implements CampaignService {

    private final CampaignDAO campaignDAO;
    private final Initializrs initializrs;
    private final DeviceService deviceService;
    private final FileMetaDataDAO metaDataDAO;
    private final StorageService storageService;
    private final ScheduleService scheduleService;


    public CampaignServiceImp(CampaignDAO campaignDAO, Initializrs initializrs, DeviceService deviceService, FileMetaDataDAO metaDataDAO, StorageService storageService, ScheduleService scheduleService) {
        this.campaignDAO = campaignDAO;
        this.initializrs = initializrs;
        this.deviceService = deviceService;
        this.metaDataDAO = metaDataDAO;
        this.storageService = storageService;
        this.scheduleService = scheduleService;
    }

    @Transactional
    @Override
    public void saveOrUpdatePlayList(PlayListEntity playListEntity) {
        calculateDurations(playListEntity);
        if (playListEntity.getPlayListId() != 0) {
            campaignDAO.updatePlayList(playListEntity);
            refreshPlayListCampaigns(playListEntity, true);
        } else
            campaignDAO.savePlayList(playListEntity);

        playListEntity.getData().getEntities().
                forEach(dataEntity -> metaDataDAO.saveMetaData(new MetaDataEntity(
                        Utils.decodeString(dataEntity.geteFile().geteFileId()),
                        PlayListEntity.class.getName() + "@" + playListEntity.getPlayListId())));


    }

    private void calculateDurations(PlayListEntity playListEntity) {
        playListEntity.getData().setDuration(0);

        playListEntity.getData().getEntities()
                .forEach(dataEntity -> playListEntity.getData().setDuration(
                        playListEntity.getData().getDuration() + dataEntity.getDuration()));

        playListEntity.setDuration(playListEntity.getData().getDuration());
    }

    @Override
    public List<PlayListEntity> getUserPlayLists(long userId) {
        return campaignDAO.getUserPlayLists(userId);
    }

    @Override
    public PlayListEntity getPlayListById(long playListId) {
        return campaignDAO.getPlayListById(playListId);
    }


    @Transactional
    @Override
    public void deletePlayListById(long playListId) {
        PlayListEntity playListEntity = campaignDAO.getPlayListById(playListId);
        if (playListEntity == null)
            throw new NotFoundException();

        refreshPlayListCampaigns(playListEntity, false);

        campaignDAO.deletePlayList(playListId);

        playListEntity.getData().getEntities().forEach(dataEntity -> {
            MetaDataEntity entity = new MetaDataEntity(Utils.decodeString(dataEntity.geteFile().geteFileId()),
                    PlayListEntity.class.getName() + "@" + playListEntity.getPlayListId());
            metaDataDAO.deleteMetaData(entity);
        });
    }

    private void refreshPlayListCampaigns(PlayListEntity playListEntity, boolean isUpdate) {
        campaignDAO.getUserCampaigns(playListEntity.getUserId())
                .forEach(campaignEntity -> {
                    List<PlayListView> selectedViews = campaignEntity.getData()
                            .getViews().stream()
                            .filter(basicView -> basicView instanceof PlayListView)
                            .map(basicView -> (PlayListView) basicView)
                            .filter(playListView -> playListView.getPlayListId() == playListEntity.getPlayListId())
                            .collect(Collectors.toList());
                    if (!selectedViews.isEmpty()) {
                        if (!isUpdate)
                            campaignEntity.getData().getViews().removeAll(selectedViews);

                        initCampaignData(campaignEntity);
                        saveOrUpdateCampaign(campaignEntity);
                    }
                });
    }

    @Override
    public CampaignEntity obtainCampaign(long userId, long campaignId) {

        CampaignEntity campaignEntity;
        if (campaignId != 0) {
            campaignEntity = campaignDAO.getCampaignById(campaignId);
            if (campaignEntity == null) throw new NotFoundException();
            if (campaignEntity.getUserId() != userId) throw new ForbiddenException();
            campaignDAO.detach(campaignEntity);
        } else {
            campaignEntity = new CampaignEntity();
            campaignEntity.setUserId(userId);
        }

        return campaignEntity;
    }

    @Override
    public void initCampaignData(CampaignEntity campaignEntity) {
        LayoutEntity layoutEntity = campaignDAO.getLayoutById(campaignEntity.getLayoutId());
        if (layoutEntity == null || (layoutEntity.getUserId() != null && !layoutEntity.getUserId().equals(campaignEntity.getUserId())))
            throw new NotFoundException();

        Layout layout = new Layout(layoutEntity.getLayoutData());
        ContentHolder holder = new ContentHolder(layout);


        //Views
        campaignEntity.setDuration(0);
        campaignEntity.getData().getViews().forEach(view -> {
            Region region = layout.findRegionById(view.getId());
            if (region == null) throw new RuntimeException("This Layout does not contain region " + view.getId());

            ViewInitializr viewInitializr = initializrs.getViewInitializrForView(view);
            if (viewInitializr != null) {
                int viewDuration = viewInitializr.doInit(view,campaignEntity);
                if (viewDuration != -1)
                    campaignEntity.setDuration(
                            campaignEntity.getDuration() < viewDuration ? viewDuration : campaignEntity.getDuration());
            }

            holder.assignViewToRegion(region, view);
        });

        if (campaignEntity.getDuration() < 60) campaignEntity.setDuration(60);

        //Options
        if (campaignEntity.getData().getOptions()!=null) {
            for (BasicOption option : campaignEntity.getData().getOptions()) {
                OptionInitializr optionInitializr = initializrs.getOptionInitializrForOption(option);
                if (optionInitializr != null)
                    optionInitializr.doInit(option,campaignEntity);
            }
        }
        holder.setOptions(campaignEntity.getData().getOptions());

        campaignEntity.setAndroidData(holder.generateData());
    }

    @Transactional
    @Override
    public void saveOrUpdateCampaign(CampaignEntity campaignEntity) {
        if (campaignEntity.getCampaignId() == 0)
            campaignDAO.saveCampaign(campaignEntity);
        else {
            campaignDAO.updateCampaign(campaignEntity);
            updateCampaignTimelines(campaignEntity);
        }
    }

    private void updateCampaignTimelines(CampaignEntity campaignEntity) {
        scheduleService.getUserTimelinesWithSeries(campaignEntity.getUserId()).stream()
                .filter(timeLineEntity ->
                        timeLineEntity.getSeries().stream()
                                .anyMatch(series -> series.getCampaigns().stream()
                                        .anyMatch(seriesCampaign -> seriesCampaign.getCampaignId() == campaignEntity.getCampaignId())))
                .forEach(scheduleService::saveOrUpdateTimeline);
    }

    @Transactional
    @Override
    public void deleteCampaign(long campaignId) {
        CampaignEntity campaignEntity = campaignDAO.getCampaignById(campaignId);
        if (campaignEntity == null)
            return;

        //Logic to update the timelines that contain this removed campaign
        scheduleService.getUserTimelinesWithSeries(campaignEntity.getUserId())
                .forEach(timeLineEntity -> {

                    ArrayList<Series> newSeriesList = new ArrayList<>();
                    //Loop over its series
                    timeLineEntity.getSeries().forEach(new Consumer<Series>() {
                        @Override
                        public void accept(Series series) {
                            boolean needsTriming = series.getCampaigns().stream()
                                    .filter(seriesCampaign -> seriesCampaign.getCampaignId() == campaignId)
                                    .peek(seriesCampaign -> {
                                        int durationToShift = seriesCampaign.getDuration();
                                        if (durationToShift == 0)
                                            durationToShift = campaignEntity.getDuration();
                                        seriesCampaign.setDuration(-durationToShift);
                                    }).count() != 0;

                            //Series doesnt need to be modified
                            if (!needsTriming) {
                                // add it to list and return
                                newSeriesList.add(series);
                                return;
                            }

                            barberTheSeries(series);
                            ArrayList<Series> finalSeries = new ArrayList<>();

                            series.getCampaigns().forEach(new Consumer<SeriesCampaign>() {
                                private ArrayList<SeriesCampaign> chosens = new ArrayList<>();
                                private long elapsedTime;
                                private long start = series.getFrom();

                                @Override
                                public void accept(SeriesCampaign seriesCampaign) {

                                    if (seriesCampaign.getDuration() >= 0) {
                                        chosens.add(seriesCampaign);
                                        if (series.getCampaigns().indexOf(seriesCampaign) == series.getCampaigns().size() - 1) {
                                            //is the last one
                                            makeNewSeries();
                                            return;
                                        }
                                        elapsedTime += (seriesCampaign.getDuration() == 0 ? campaignDAO.getCampaignById(campaignId).getDuration() : seriesCampaign.getDuration());
                                    } else if (!chosens.isEmpty()) {
                                        makeNewSeries();
                                        chosens.clear();
                                        start += elapsedTime + (-seriesCampaign.getDuration());
                                        elapsedTime = 0;
                                    } else {
                                        start += -seriesCampaign.getDuration();
                                    }
                                }

                                private void makeNewSeries() {
                                    Series newSeries = new Series();
                                    newSeries.setCampaigns(new ArrayList<>(chosens));
                                    newSeries.setFrom(start);
                                    newSeries.setDelimiter(Series.DELIMITER_END);
                                    finalSeries.add(newSeries);
                                }
                            });

                            if (!finalSeries.isEmpty())
                                finalSeries.get(finalSeries.size() - 1).setDelimiter(series.getDelimiter());

                            newSeriesList.addAll(finalSeries);
                        }

                        private void barberTheSeries(Series series) {
                            LinkedList<SeriesCampaign> linkedCampaigns = new LinkedList<>(series.getCampaigns());

                            //remove the removed campaign from the tail
                            do {
                                if (linkedCampaigns.getLast().getDuration() < 0)
                                    linkedCampaigns.removeLast();
                                else break;
                            } while (!linkedCampaigns.isEmpty());

                            //remove the removed campaign from the head
                            while (!linkedCampaigns.isEmpty()) {
                                if (linkedCampaigns.getFirst().getDuration() < 0) {
                                    series.setFrom(series.getFrom() + (-linkedCampaigns.getFirst().getDuration()));
                                    linkedCampaigns.removeFirst();
                                } else break;
                            }

                            series.setCampaigns(new ArrayList<>(linkedCampaigns));
                        }

                    });

                    //end of loop
                    if (!newSeriesList.equals(timeLineEntity.getSeries())) {
                        timeLineEntity.setSeries(newSeriesList);
                        scheduleService.saveOrUpdateTimeline(timeLineEntity);
                    }

                });

        campaignDAO.deleteCampaign(campaignId);

    }


    @Override
    public CampaignEntity getCampaignById(long campaignId) {
        return campaignDAO.getCampaignById(campaignId);
    }

    @Override
    public List<CampaignEntity> getUserCampaigns(long userId) {
        return campaignDAO.getUserCampaigns(userId);
    }

    @Override
    public long getUserCampaignCount(long userId) {
        return campaignDAO.getUserCampaignCount(userId);
    }

    @Transactional
    @Override
    public void saveLayout(LayoutEntity layoutEntity) {
        campaignDAO.saveLayout(layoutEntity);
    }

    @Override
    public LayoutEntity getLayoutById(long layoutId) {
        return campaignDAO.getLayoutById(layoutId);
    }

    @Override
    public List<LayoutEntity> getUserAvailableLayouts(long userId, String orientation) {
        List<LayoutEntity> layoutEntities = campaignDAO.getUserLayouts(userId, orientation);
        layoutEntities.addAll(campaignDAO.getGlobalLayouts(orientation));

        return layoutEntities;
    }

    @Override
    public List<LayoutEntity> getUserLayouts(long userId) {
        return campaignDAO.getUserLayouts(userId);
    }

    @Transactional
    @Override
    public void deleteLayout(long layoutId) {
        campaignDAO.deleteLayout(layoutId);
    }

    @Async
    @EventListener(classes = StorageService.FileDeletedEvent.class)
    @Transactional
    public void onEvent(StorageService.FileDeletedEvent event) {
        String fileId = storageService.generateFileId(event.getFile());
        metaDataDAO.getFileMetaDatas(fileId)
                .forEach(metaDataEntity -> {
                    if (metaDataEntity.getReference().startsWith(PlayListEntity.class.getName())) {
                        long playListId = Long.parseLong(metaDataEntity.getReference().replace(PlayListEntity.class.getName() + "@", ""));
                        PlayListEntity playListEntity = campaignDAO.getPlayListById(playListId);
                        if (playListEntity != null) {
                            if (playListEntity.getData().getEntities().removeIf(dataEntity ->
                                    Utils.decodeString(dataEntity.geteFile().geteFileId()).equals(fileId)))
                                saveOrUpdatePlayList(playListEntity);
                        }
                        metaDataDAO.deleteMetaData(metaDataEntity);
                    }
                });
    }


}
