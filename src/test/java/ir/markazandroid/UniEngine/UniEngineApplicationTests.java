package ir.markazandroid.UniEngine;

import ir.markazandroid.UniEngine.controller.userApi.interfaces.UserApiAuthenticationController;
import ir.markazandroid.UniEngine.media.layout.Layout;
import ir.markazandroid.UniEngine.media.layout.Orientation;
import ir.markazandroid.UniEngine.persistance.entity.DeviceEntity;
import ir.markazandroid.UniEngine.persistance.entity.LayoutEntity;
import ir.markazandroid.UniEngine.persistance.entity.ScheduleEntity;
import ir.markazandroid.UniEngine.persistance.entity.UserEntity;
import ir.markazandroid.UniEngine.persistance.interfaces.DeviceDAO;
import ir.markazandroid.UniEngine.persistance.interfaces.ScheduleDAO;
import ir.markazandroid.UniEngine.service.interfaces.CampaignService;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UniEngineApplicationTests {

    @Autowired
    private CampaignService campaignService;


    @Autowired
    private ScheduleDAO scheduleDAO;

    @Transactional
    @Test
    public void test5() {
        ScheduleEntity entity = scheduleDAO.getFullSchedule(1);
        entity = null;
    }

    @Test
    public void saveLayout() {
        Layout layout = new Layout();
        layout.setOrientation(Orientation.vertical);
        layout.newRegion(layout.getLeftParentSide(), layout.getTopParentSide(), layout.getRightParentSide(), layout.getBottomParentSide());

        LayoutEntity layoutEntity = new LayoutEntity();
        layoutEntity.setOrientation(layout.getOrientation().name());
        layoutEntity.setLayoutData(layout.toLayoutData());

        campaignService.saveLayout(layoutEntity);

        loadLayout();
    }

    /* vertical_0_horizontal_50_vertical_50_horizontal_100
             vertical_50_horizontal_0_vertical_100_horizontal_50
     vertical_0_horizontal_0_vertical_50_horizontal_50
             vertical_50_horizontal_50_vertical_100_horizontal_100*/
    @Test
    public void loadLayout() {
        LayoutEntity layoutEntity = campaignService.getLayoutById(7);
        Layout layout = new Layout(layoutEntity.getLayoutData());

        layout.getRegions().forEach(region -> System.out.println(region.getId()));

    }

    @Autowired
    private DeviceDAO deviceDAO;

    @Transactional
    @Test
    public void testDevice() {
        DeviceEntity deviceEntity = new DeviceEntity();
        //deviceEntity.setUuid(UUID.randomUUID().toString());

        //deviceDAO.saveDevice(deviceEntity);

        deviceEntity = deviceDAO.getDeviceById(298, "uuid", "name");

        deviceEntity = null;

    }


    public static void main(String[] args) {
        JSONObject object = new JSONObject();
        object.put("ali", "ali\nfew\r\nfesfd");
        object.put("fdsf", 1);
        System.out.println(object);
        System.out.println("ali\nfew\r\nfesfd");
    }

    @Autowired
    public UserApiAuthenticationController userApiAuthenticationController;


    @Test
    public void testRegister(){
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName("علی");
        userEntity.setLastName("غلامی");
        userEntity.setCompanyName("مبین فناوران جی");
        userEntity.setMeliCode("10260591382");
        userEntity.setEmail("alig.mym@dr.com");
        userEntity.setPhone("09137417731");
        userEntity.setPassword("12345678");
        userEntity.setType(UserEntity.USER_TYPE_COMPANY);
        System.out.println(userApiAuthenticationController.register(userEntity).getBody().toString());
    }

}
